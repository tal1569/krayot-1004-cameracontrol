#pragma once

#include <iostream>
#include <string>
#include <string.h>
#include "Helper.h"

using std::string;

#define NUM_OF_FINGERS 5

class Gesture
{
public:
	Gesture(int id, int numOfFingers, bool fingers[NUM_OF_FINGERS], string action);
	~Gesture();
	bool compare(Gesture other);
	int getId();
	int getNumOfFingers();
	bool* getFingers();
	string getAction();
	string toString();

private:
	int _id;
	int _numOfFingers;
	bool _fingers[NUM_OF_FINGERS];
	string _action;
};