#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>
#include <algorithm>
#include <map>
#include "Protocol.h"
#include <iostream>
#include <string.h>

#define DATABASE_NAME "camera_control.db"  // the database name in the database server

using namespace std;

#include "sqlite3.h"
#include "Gesture.h"


class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(std::string);
	bool addNewUser(std::string, std::string, std::string);
	bool isUserAndPassMatch(std::string, std::string);

	vector <Gesture*> getAllGestures(string username);
	bool doesGestureExists(string username, int numOfFingersRaised, string fingersRaised);
	bool createGesture(string username, int numOfFingersRaised, string fingersRaised, string action);
	bool doesGestureBelongToUser(string username, int id);
	int deleteGesture(int id);
	int editGestureAction(string username, string fingers, string action);

	string getUserPassword(string username);
	string getUserEmail(string username);
	void changeUserPassword(string username, string password);

	int getLastGestureId();

private:
	sqlite3* _db;
	static int callback(void* notUsed, int argc, char** argv, char** azCol);
	static void clearTable();
};
