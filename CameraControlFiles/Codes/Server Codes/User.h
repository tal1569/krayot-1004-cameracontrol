#pragma once
#ifndef USER_H
#define USER_H
#include <string>
#include <WinSock2.h>
#include "Helper.h"


#define MINUS -1

using std::string;

class User
{
public:
	User(string username, SOCKET sock);
	~User();
	void send(string message);
	string getUsername();
	SOCKET getSocket();
private:
	string _username;
	SOCKET _sock;
};
#endif // !USER_H

