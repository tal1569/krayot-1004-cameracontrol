#include "CameraWindow.h"

/*
Function is the c'tor of the class
Input: the parent, whether or not the user is connected to the server, the client and guest objects
Output: none
*/
CameraWindow::CameraWindow(QWidget *parent, bool onlineMode, Client* client, Guest* guest) : QMainWindow()
{
	this->_ui.setupUi(this);
	this->_prevWindow = parent;
	this->_client = client;
	this->_guest = guest;
	this->_onlineMode = onlineMode;
	this->_cameraFlag = true;

	try
	{
		//start the camera module
		int cameraIndex = atoi(readConfigFile()[2].c_str());
		this->_camera = new Camera(cameraIndex, this);
	}
	catch (...)
	{
		Menu* form = new Menu(nullptr, this->_onlineMode, this->_client, this->_guest);
		QMessageBox* messageBox = new QMessageBox(form);
		messageBox->setText("Camera Error");
		messageBox->setWindowTitle("Can't open the camera");
		messageBox->addButton(QMessageBox::Ok);
		messageBox->setStyleSheet("image:none");
		messageBox->show();

		this->hide();
		form->show();
		return;
	}
}

/*
Function will handle the "Sign Out" button
Input: none
Output: none
*/
void CameraWindow::signOutHandle()
{
	MainWindow* main = new MainWindow();
	main->show();
	this->hide();
	delete this;
}

/*
Function will handle the push of the "x" button
Input: none
Output: none
*/
void CameraWindow::closeEvent(QCloseEvent *event)
{
	QMessageBox* messageBox = new QMessageBox(this);
	messageBox->setText("Are you sure?");
	messageBox->setWindowTitle("Exit");
	messageBox->addButton(QMessageBox::No);
	messageBox->addButton(QMessageBox::Yes);

	messageBox->setStyleSheet("image:none");
	messageBox->show();

	int resBtn = messageBox->exec();

	if (resBtn != (int)(QMessageBox::Yes))
	{
		event->ignore();
	}
	else
	{
		if (this->_onlineMode == true)
			this->_client->signOut();

		this->_camera->disconnectForm();
		cv::destroyAllWindows();

		event->accept();
	}
}

/*
Function will set the picture on the form
Input: the image
Output: none
*/
void CameraWindow::setPicture(QImage* img)
{
	//ui.image_lbl.setPixmap(QPixmap::fromImage(img));
	if (this->_cameraFlag)
		this->_ui.image->setPixmap(QPixmap::fromImage(*img));
}

/*
Function will handle the "start camera" button
Input: none
Output: none
*/
void CameraWindow::startHandle()
{
	this->_camera->cameraManager(this->_onlineMode, false, this->_client, this->_guest);
	this->_ui.start->setEnabled(false);
}

/*
Function will handle the "back" button
Input: none
Output: none
*/
void CameraWindow::backHandle()
{
	this->_cameraFlag = false;
	//close camera.
	this->_camera->disconnectForm();
	this->hide();
	this->_prevWindow->show();
	delete this;
}

/*
Function will set the label on the form
Input: the message to set
Output: none
*/
void CameraWindow::setLabel(string msg)
{
	this->_ui.image->setText(QString(msg.c_str()));
}