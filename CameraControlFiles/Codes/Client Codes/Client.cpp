#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "Client.h"

/*
Function is the c'tor of the class
Input: the index of the camera and the form
Output: none
*/
Client::Client(int cameraIndex, QWidget* form)
{
	this->_form = form;
	this->_cameraIndex = cameraIndex;
	this->_exit = false;
	this->_signedIn = false;

	// notice that we step out to the global namespace
	// for the resolution of the function socket

	// we connect to server that uses TCP. thats why SOCK_STREAM & IPPROTO_TCP
	_clientSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_clientSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

}

/*
Function is the d'tor of the class
*/
Client::~Client()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_clientSocket);
	}
	catch (...) {}
}

/*
Function will connect to the server
Input: the ip of the server and the port
Output: none
*/
void Client::connect(string serverIP, int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen to
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr =  inet_addr(serverIP.c_str());    // the IP of the server

	int status = ::connect(_clientSocket, (struct sockaddr*)&sa, sizeof(sa));

	if (status == INVALID_SOCKET)
		throw std::exception("Can't connect to server");
}

/*
Function will start the conversation with the server
Input: none
Output: none
*/
void Client::startConversation()
{
	std::thread t2(&Client::listening, this);
	t2.detach();
}

/*
Function will listen to the messages of the server and will function accordingly
Input: none
Output: none
*/
void Client::listening()
{
	while (!this->_exit)
	{
		char m[1025];
		recv(_clientSocket, m, 1024, 0);
		m[1024] = 0;

		string message(m); //turning the message from char* to string

		string msgCode = message.substr(0, 3); //taking the first 3 bytes of the message (the message code)

		if(message.size() > 3)
			message = message.substr(3);

		//Checking what kind of message we got

		if (msgCode.compare(SIGN_IN_ANS) == 0)
		{	
			if(this->_signedIn == false)
				((SignIn*)this->_form)->signinStatus(message);
			
			if (message[0] == '0') //Success, even great success even even
			{
				this->_signedIn = true;
			}
		}
		else if (msgCode.compare(SIGN_UP_ANS) == 0)
		{
			((SignUp*)this->_form)->signupStatus(message);
		}
		else if (msgCode.compare(SEND_GESTURES) == 0)
		{
			
			bool fingers[NUM_OF_FINGERS];

			int numOfGestures = atoi(message.substr(0, 4).c_str()); //Getting the number of gestures that were sent to the user
			message = message.substr(4);

			this->_userGestures.clear();

			for (int i = 0; i < numOfGestures; i++)
			{
				int id = atoi(message.substr(0, 4).c_str()); //Getting the id
				message = message.substr(4);

				int numOfFingersRaised = atoi(message.substr(0, 1).c_str()); //Getting the number of fingers raised
				message = message.substr(1);

				string indexOfFingersRaised = message.substr(0, 5); // Getting the indexes of the fingers that are raised
				message = message.substr(5);

				for (int j = 0; j < indexOfFingersRaised.length(); j++)
				{
					if (indexOfFingersRaised[j] != '0')
					{
						fingers[j] = true;
					}
					else
					{
						fingers[j] = false;
					}
				}

				int actionLength = atoi(message.substr(0, 3).c_str()); //Getting the length of the action
				message = message.substr(3);

				string action = message.substr(0, actionLength); //Getting the action
				message = message.substr(actionLength);

				this->_userGestures.push_back(new Gesture(id, numOfFingersRaised, fingers, action));
			}
		}
		else if (msgCode.compare(EDIT_GESTURE_ACTION_ANSWER) == 0)
		{
			int idSize = atoi(message.substr(0, 2).c_str());
			message = message.substr(2);

			int id = atoi(message.substr(0, idSize).c_str());
			message = message.substr(idSize);

			if (id != 0)
			{
				int actionSize = atoi(message.substr(0, 2).c_str());
				message = message.substr(2);

				string action = message.substr(0, actionSize);

				for (int i = 0; i < this->_userGestures.size(); i++)
				{
					if (this->_userGestures[i]->getId() == id)
					{
						this->_userGestures[i]->setAction(action);
						break;
					}
				}
			}
		}
		else if (msgCode.compare(FORGOT_PASSWORD_ANS) == 0)
		{
			if (message[0] == '0') //Success
			{
				std::cout << "Success" << std::endl;
			}
			else if (message[0] == '1') //Could not send email
			{
				std::cout << "Could not send the email" << std::endl;
			}
			else //User does not exist or other reason
			{
				std::cout << "User does not exist or other reason" << std::endl;
			}
		}
		else if (msgCode.compare(CREATE_GESTURE_ANS) == 0)
		{
			if (message[0] == '0') //Success
			{
				//Now we need to change the value of gesture id

				message = message.substr(1);
				int idSize = atoi(message.substr(0, 2).c_str()); //Getting the size of the id
				message = message.substr(2);
				int id = atoi(message.substr(0, idSize).c_str()); //Getting the id

				if (this->_userGestures.size() > 0)
				{
					Gesture* temp = this->_userGestures[this->_userGestures.size() - 1];

					this->_userGestures.pop_back();
					this->_userGestures.push_back(new Gesture(id, temp->getNumOfFingers(), temp->getFingers(), temp->getAction()));
				}

				((AddAction*)this->_form)->addGestureStatus(true);
			}
			else //Failure
			{
				((AddAction*)this->_form)->addGestureStatus(false);
				//The gesture was not added to the server's database, so we need to remove it from our list
				this->_userGestures.pop_back();
			}
		}
		else if (msgCode.compare(DELETE_GESTURE_ANS) == 0)
		{
			if (message[0] == '0') //Success
			{
				//The gesture was successfuly removed in the server, so we need to do the same here
				message = message.substr(1);
				int idSize = atoi(message.substr(0, 2).c_str()); //Getting the size of the id
				message = message.substr(2);
				int id = atoi(message.substr(0, idSize).c_str()); //Getting the id

				for (int i = 0; i < this->_userGestures.size(); i++)
				{
					if (id == this->_userGestures[i]->getId()) //Checking if we found a match
					{
						this->_userGestures.erase(this->_userGestures.begin() + i);
					}
				}
			}
		}
	}
}

/*
Function will send the sign in message to the server
Input: the username and password
Output: Status of sending
*/
int Client::signin(string username, string password)
{
	this->_username = username;
	string message;

	message = SIGN_IN; //Starting building the message

	message = message + Helper::getPaddedNumber(username.size(), 2) + username; //Adding the username size and username to the message

	message = message + Helper::getPaddedNumber(password.size(), 2) + password; //Adding the password size and password to the message

	return send(_clientSocket, message.c_str(), message.size(), 0); //Sending the sign in message
}

/*
Function will send the sign up message to the server
Input: the username, password and email
Output: Status of sending
*/
int Client::signup(string username, string password, string email)
{
	string message;

	message = SIGN_UP; //Starting to build the message

	message = message + Helper::getPaddedNumber(username.size(), 2) + username; //Adding the username size and the username to the message

	message = message + Helper::getPaddedNumber(password.size(), 2) + password; //Adding the password size and the password to the message

	message = message + Helper::getPaddedNumber(email.size(), 2) + email; //Adding the email size and the email to the message

	return send(this->_clientSocket, message.c_str(), message.size(), 0); //Sending the sign up message
}

/*
Function will send the "forgot password" message to the server
Input: the username
Output: Status of sending
*/
int Client::forgotPassword(string username)
{
	string message;

	message = FORGOT_PASSWORD; //Starting to build the message

	message = message + Helper::getPaddedNumber(username.size(), 2) + username; //Adding the username size and the username to the message

	return send(this->_clientSocket, message.c_str(), message.size(), 0);
}

/*
Function will send the exit message to the server
Input: none
Output: Status of sending
*/
int Client::exitApp()
{
	string message;

	message = EXIT_APP;

	this->_signedIn = false;
	this->_exit = true;

	return send(this->_clientSocket, message.c_str(), message.size(), 0);
}

/*
Function will send the server the message that ask it to send back all the gestures of the user
Input: none
Output: Status of sending
*/
int Client::getAllGestures()
{
	string message;

	message = GET_GESTURES;
	return send(this->_clientSocket, message.c_str(), message.size(), 0);
}

/*
Function will send the add gesture message to the server
Input: the username, path, number of fingers raised, index of the raised fingers and the action
Output: Status of sending
*/
int Client::addGesture(Gesture* gesture)
{
	string message = CREATE_GESTURE;

	message = message + Helper::getPaddedNumber(this->_username.size(), 2) + this->_username; //Adding the username's size and the username to the message
	message = message + Helper::getPaddedNumber(gesture->getNumOfFingers(), 1); //Adding the number of the raised fingers to the message
	message = message + gesture->getFingersIndex(); //Adding the indexes of the raised fingers to the message
	message = message + Helper::getPaddedNumber(gesture->getAction().size(), 3) + gesture->getAction(); //Adding the action's size and the action to the message

	this->_userGestures.push_back(gesture);

	return send(this->_clientSocket, message.c_str(), message.size(), 0);
}

/*
Function will send the delete gesture message to the server
Input: Id
Output: Status of sending
*/
int Client::deleteGesutre(int gestureId)
{
	string message;

	message = DELETE_GESTURE;

	message = message + Helper::getPaddedNumber(this->_username.size(), 2) + this->_username; //Adding the username size and the username to the message
	message = message + Helper::getPaddedNumber(std::to_string(gestureId).size(), 3) + std::to_string(gestureId); //Adding the id size and the id to message

	return send(this->_clientSocket, message.c_str(), message.size(), 0);
}

/*
Function will send the sign out message to the server
Input: none
Output: Status of sending
*/
int Client::signOut()
{
	string message = SIGN_OUT;

	return send(this->_clientSocket, message.c_str(), message.size(), 0);
}

/*
Function will execute the action
Input: the id of the gesture
Output: none
*/
void Client::exeAction(int id)
{
	WindowsControl wc;

	for (int i = 0; i < this->_userGestures.size(); i++)
	{
		if (id == this->_userGestures[i]->getId()) //Checking if we found a match
		{
			string action = this->_userGestures[i]->getAction();
			wc.manageAction(action);
		}
	}
}

/*
Function will compare a gesture to all of the gestures of the user
Input: other gesture
Output: none
*/
void Client::compareGesture(Gesture* gesture)
{
	WindowsControl wc;

	for (int i = 0; i < this->_userGestures.size(); i++)
		if (this->_userGestures[i]->compareByFingers(gesture))
			wc.manageAction(this->_userGestures[i]->getAction());
}

/*
Function will edit an action of a gesture
Input: the fingers raised of the gesture and the action
Output: Status of sending
*/
int Client::editAction(string action, string fingers)
{
	int choise;
	std::string filePath;

	string message = EDIT_GESTURE_ACTION;

	message = message + Helper::getPaddedNumber(this->_username.size(), 2) + this->_username + fingers + Helper::getPaddedNumber(action.size(), 3) + action;

	return send(this->_clientSocket, message.c_str(), message.size(), 0);
}

/*
Function will return all the gestures of the user
Input: none
Output: the gestures of the user
*/
std::vector<Gesture*> Client::getAllLocalGestures()
{
	return this->_userGestures;
}

/*
Function will set the form attribute to a form given as a parameter
Input: the form
Output: none
*/
void Client::setForm(QWidget* form)
{
	this->_form = form;
}