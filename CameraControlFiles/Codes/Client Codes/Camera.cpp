#include "Camera.h"

/*
	The c'tor of Camera
	Input: the index of the camera and the form
	Output: none
*/
Camera::Camera(int cameraIndex, QWidget* form)
{
	cv::VideoCapture temp(cameraIndex);
	this->cap = temp;
	if (cap.isOpened() == false)
		throw ("Can't open camera");

	if (this->_faceCascade.load(this->_faceCascadeName) == false) {
		printf("--(!)Error loading\n");
		//need to make exeption
	}

	this->_keyFrame = KEY_FRAME;

	this->_handDetection = new HandDetection();

	this->_cameraForm = form;

	this->_formFlag = true;

	this->_detectionGesture = nullptr;
}

/*
	The d'tor of Camera
	Input: none
	Output: none
*/
Camera::~Camera()
{

}

/*
Function will release the camera
Input: none
Output: none
*/
void Camera::releaseCamera()
{
	this->cap.release();
}

/*
	The function manage the camera module
	Input: whether or not the user is connected to the server, the client and guest objects and whether or not the user wants to add a gesture
	Output: a gesture pointer
*/
Gesture* Camera::cameraManager(bool onlineMode,bool addAction, Client* client, Guest* guest)
{
	cap >> this->_backgroundImage;
	flip(this->_backgroundImage, this->_backgroundImage, 1);
	cv::Mat frame, bwFrame, YFrame, CrFrame, CbFrame, maskedFrame, bwRGB, BGRMax;
	bool flag = false;
	this->_isFace = false;

	while (!flag && this->_formFlag == true)
	{
		
		cap >> this->_originalFrame;

		flip(this->_originalFrame, this->_originalFrame, 1);

		if (this->_keyFrame == KEY_FRAME)
		{
			try
			{
				faceDetection();
				this->_keyFrame = 0;
				this->_isFace = true;
			}
			catch (...)
			{
				this->_isFace = false;
			}
		}
		//convert the frame that we get to YCrCb format.
		cvtColor(this->_originalFrame, this->_YCrCbFrame, cv::COLOR_BGR2YCrCb);

		//remove the background from the YCrCb frame.
		bwFrame = backgroundSubstraction(this->_YCrCbFrame);

		//Morphology (on bw image with opening operator)
		bwFrame = morphology(bwFrame);

		//mask the main frame with the results
		maskedFrame = maskFrame(bwFrame);


		//remove the face on the YCrCb frame if the face has detected
		if(this->_isFace)	//if the face was found, remove the face from the frame ; else skip this step
			maskedFrame = faceRemove(maskedFrame);

		maskedFrame = morphology(maskedFrame);
		//find the skin color by the YCrCb values that shows in Fig.5 and hide anything else
		bwFrame = skinExtraction(maskedFrame);
		bwFrame = morphology(bwFrame);

		cvtColor(bwFrame, bwRGB, cv::COLOR_YCrCb2BGR);
		BGRMax = maxRgbFilter(bwRGB);

		//Morphology (on bw image with opening operator)
		this->_bwHandFrame = morphology(this->_bwHandFrame);

		//Gaussian filter
		medianBlur(this->_bwHandFrame, this->_bwHandFrame, 5);
		GaussianBlur(this->_bwHandFrame, this->_bwHandFrame, cv::Size(5, 5), 0, 0);
		//Send to the detaction module
		Gesture* gesture = this->_handDetection->detectHand(this->_bwHandFrame,onlineMode,client,guest,addAction,this->_cameraForm);

		if (gesture != nullptr)
			this->_detectionGesture = gesture;


		//show the frames
		///imshow("frame", this->_originalFrame);
		imshow("bw hand", this->_bwHandFrame);

		this->_keyFrame++;

		//send the image to camera form
		//setImageOnForm();


		int option = cv::waitKey(30);
		if (option == 'q')
			flag = true;
		else if (option == 'b')
		{
			cv::waitKey(1000);
			cap >> this->_backgroundImage;
			flip(this->_backgroundImage, this->_backgroundImage, 1);
			std::cout << "update back" << std::endl;
			cv::waitKey(5);
		}
		else if (option == 'A' || option == 'a' && addAction == true)
		{
			//return the hand deteils to add it to db
			cv::destroyAllWindows();
			releaseCamera();
			return this->_detectionGesture;
		}
	}

	cv::destroyAllWindows();
	releaseCamera();
	return NULL;
}

/*
	The function set the beckground image in the camera class
	Input: new beckground image
	Output: none
*/
void Camera::setBackgroundImage(cv::Mat backgroundImage)
{
	this->_backgroundImage = backgroundImage;
}



/*
	The function remove the background from the image that he get.
	Input: frame to remove the background.
	Output: the same frame that the function get but without the background.
*/
cv::Mat Camera::backgroundSubstraction(cv::Mat frame)
{
	cv::Mat frametemp = frame.clone();
	cv::Mat backgroundTemp;
	cvtColor(this->_backgroundImage, backgroundTemp, cv::COLOR_BGR2YCrCb);

	int Y, Cr, Cb;

	for (int i = 0; i < (frametemp.rows); i++)
	{
		for (int j = 0; j < (frametemp.cols); j++)
		{
			cv::Vec3b& backYCrCb = backgroundTemp.at<cv::Vec3b>(i, j);
			cv::Vec3b& frameYCrCb = frametemp.at<cv::Vec3b>(i, j);

			Y = abs(frameYCrCb[0] - backYCrCb[0]);
			Cr = abs(frameYCrCb[1] - backYCrCb[1]);
			Cb = abs(frameYCrCb[2] - backYCrCb[2]);

			if (Y < 20 && Cr < 20 && Cb < 20)
			{
				frameYCrCb[0] = 0;
				frameYCrCb[1] = 0;
				frameYCrCb[2] = 0;
			}
			else
			{
				frameYCrCb[0] = 255;
				frameYCrCb[1] = 255;
				frameYCrCb[2] = 255;
			}
			
		}
	}
	return frametemp;
}



/*
	The function do opening morphology on the frame that she get
	Input: Frame to make opening morphology
	Output: Frame after opening morphology
*/
cv::Mat Camera::morphology(cv::Mat frame)
{
	cv::Mat temp;
	cv::Mat kernel = cv::Mat::ones(5, 5, CV_32F);
	
    cv::morphologyEx(frame, temp, cv::MORPH_OPEN, kernel);
		
	return temp;
}

/*
	The function detecte the user face from the original frame.
	Input: none
	Output: none
*/
void Camera::faceDetection()
{
	cv::Mat frame_gray;
	std::vector<cv::Rect> faces;

	cvtColor(this->_originalFrame, frame_gray, cv::COLOR_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);

	try
	{
		// Detect faces
		this->_faceCascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | cv::CASCADE_SCALE_IMAGE, cv::Size(30, 30));

		// Set Region of Interest
		cv::Rect roi_b;
		cv::Rect roi_c;

		size_t ic = 0; // ic is index of current element
		int ac = 0; // ac is area of current element

		size_t ib = 0; // ib is index of biggest element
		int ab = 0; // ab is area of biggest element

		for (ic = 0; ic < faces.size(); ic++) // Iterate through all current elements (detected faces)
		{
			roi_c.x = faces[ic].x;
			roi_c.y = faces[ic].y;
			roi_c.width = (faces[ic].width);
			roi_c.height = (faces[ic].height);

			ac = roi_c.width * roi_c.height; // Get the area of current element (detected face)

			roi_b.x = faces[ib].x;
			roi_b.y = faces[ib].y;
			roi_b.width = (faces[ib].width);
			roi_b.height = (faces[ib].height);

			ab = roi_b.width * roi_b.height; // Get the area of biggest element, at beginning it is same as "current" element

			if (ac > ab)
			{
				ib = ic;
				roi_b.x = faces[ib].x;
				roi_b.y = faces[ib].y;
				roi_b.width = (faces[ib].width);
				roi_b.height = (faces[ib].height);
			}

		}

		if(faces.size() != 0)
			this->_face = faces[ib];
	}
	catch (...)
	{

	}
}

/*
	The function remove the face that detected in the frame that she get.
	Input: frame to remove the face
	Output: frame after face removal
*/
cv::Mat Camera::faceRemove(cv::Mat frame)
{
	cv::Point pt1(this->_face.x, this->_face.y); // Display detected faces on main window
	cv::Point pt2((this->_face.x + this->_face.height), (this->_face.y + this->_face.width));
	rectangle(frame, pt1, pt2, cv::Scalar(0, 0, 0),-1 , 8, 0);

	return frame;
}

/*
Function will extract the skin color from the frame
Input: the frame
Output: a mat
*/
cv::Mat Camera::skinExtraction(cv::Mat frame)
{
	cv::Mat YCrCb[3];
	cv::Mat temp[3];

	cv::split(frame, YCrCb);

	cv::Mat frameTemp;
	threshold(frame, frameTemp, 130, 255, cv::THRESH_BINARY);


	return frameTemp;
}

/*
	The function mask the original frame with the bwframe and return copy of the original frame
	Input: black and white image for make a mask on original frame.
	Output: copy of the original frame with mask
*/
cv::Mat Camera::maskFrame(cv::Mat bwFrame)
{
	cv::Mat frame = this->_YCrCbFrame.clone();
	for (int i = 0; i < frame.rows; i++)
	{
		for (int j = 0; j < frame.cols; j++)
		{
			cv::Vec3b& maskBW = bwFrame.at<cv::Vec3b>(i, j);
			cv::Vec3b& frameYCrCb = frame.at<cv::Vec3b>(i, j);

			if (maskBW[0] == 0)
			{
				frameYCrCb[0] = 0;
				frameYCrCb[1] = 0;
				frameYCrCb[2] = 0;
			}
		}
	}

	return frame;
}

/*
	The function filters the image with rgb max filter
	Input: image to do the filter on it.
	Output: the image after filter
*/
cv::Mat Camera::maxRgbFilter(cv::Mat input)
{
	cv::Mat frame = input.clone();

	this->_bwHandFrame = input.clone();

	for (int i = 0; i < frame.rows; i++)
	{
		for (int j = 0; j < frame.cols; j++)
		{
			cv::Vec3b& BGR = frame.at<cv::Vec3b>(i, j);
			cv::Vec3b& bw = this->_bwHandFrame.at<cv::Vec3b>(i, j);

			if (BGR[0] > BGR[1] && BGR[0] > BGR[2])
			{
				BGR[0] = 255;
				BGR[1] = 0;
				BGR[2] = 0;
				bw[0] = 0;
				bw[1] = 0;
				bw[2] = 0;
			}
			else if (BGR[1] > BGR[0] && BGR[1] > BGR[2])
			{
				BGR[0] = 0;
				BGR[1] = 255;
				BGR[2] = 0;
				bw[0] = 0;
				bw[1] = 0;
				bw[2] = 0;
			}
			else if (BGR[2] > BGR[0] && BGR[2] > BGR[1])
			{
				BGR[0] = 0;
				BGR[1] = 0;
				BGR[2] = 255;
				bw[0] = 255;
				bw[1] = 255;
				bw[2] = 255;
			}
		}
	}


	return frame;
}

/*
Function will set the image on the form
Input: none
Output: none
*/
void setImageOnForm(cv::Mat frame,QWidget* form)
{
	cv::Mat img;
	cv::cvtColor(frame, img, CV_BGR2RGB);

	if (form != nullptr)
	{
		((CameraWindow*)form)->setPicture(new QImage(img.data, img.cols, img.rows, img.step, QImage::Format_RGB888));
		((AddAction*)form)->setPicture(new QImage(img.data, img.cols, img.rows, img.step, QImage::Format_RGB888));
	}
}

/*
Function will disconnect the camera from the form
Input: none
Output: none
*/
void Camera::disconnectForm()
{
	this->_formFlag = false;
	this->_cameraForm = nullptr;;
}

/*
Function will return the current gesture
Input: none
Output: the gesture pointer
*/
Gesture* Camera::getCurrectGesture()
{
	return this->_detectionGesture;
}