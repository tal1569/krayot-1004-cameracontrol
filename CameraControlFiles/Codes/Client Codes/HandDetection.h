#pragma once

#include "opencv/cv.h"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <Windows.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <stack>

#include "Client.h"
#include "Guest.h"
#include "Gesture.h"
#include "InternalDB.h"
#include "CameraWindow.h"

class Guest;
class Client;

class HandDetection
{
	private:
		//private varibels
		cv::Mat _binaryImage;
		float _maxRadius;
		float _minRadius;
		cv::Point2f _maxCircle;
		cv::Point2f _minCircle;

		std::vector<cv::Point> _fingerPos;
		int _countFrame;
		int _timerCounter;

		string _message;

		std::stack<std::vector<cv::Point>>* _frameSave;

		//private functions
		bool* isFingerGone(std::vector<cv::Point> first, std::vector<cv::Point> flag);
		bool comparePoint(cv::Point first, cv::Point flag);

		std::vector<cv::Point> compactOnNeighborhoodMedian(std::vector<cv::Point> points, double max_neighbor_distance);
		double findAngle(cv::Point a, cv::Point b, cv::Point c);
		std::vector<cv::Point> findClosestOnX(std::vector<cv::Point> points, cv::Point pivot);

		bool isFinger(cv::Point a, cv::Point b, cv::Point c, double limit_angle_inf, double limit_angle_sup, cv::Point palm_center, double min_distance_from_palm);


	public:
		HandDetection();
		~HandDetection();

		void drawVectorPoints(cv::Mat image, std::vector<cv::Point> points, cv::Scalar color, bool with_numbers);

		float findPointsDistance(cv::Point2f a, cv::Point2f b);
		double findPointsDistanceOnX(cv::Point a, cv::Point b);

		Gesture* detectHand(cv::Mat bwFrame,bool onlineMode,Client* client,Guest* guest, bool addAction,QWidget* form);
		void setBinaryImage(cv::Mat binaryImage);
};