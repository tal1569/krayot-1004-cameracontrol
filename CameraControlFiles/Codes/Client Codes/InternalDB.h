#pragma once
#include <string>
#include <vector>
#include <exception>
#include <unordered_map>
#include <map>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <time.h>
#include <fstream>
#include "sqlite3.h"
#include "Gesture.h"

#define DATABASE_NAME "DataBase.db"
#define CODE_ONE 1 //Code 1 : add a gesture
#define CODE_TWO 2 //Code 2 : delete a gesture


class InternalDB
{
public:
	InternalDB();
	~InternalDB();
	bool doesGestureExist(string path);
	bool doesGestureExist(int numOfFingers, string fingers, string action);
	bool doesGestureExist(int id);
	int getLastGestureId();
	bool addGesture(int numOfFingers, string fingers, string action);
	int deleteGesture(int id);
	int editGestureAction(string fingers, string action);
	std::vector<string> getGestureById(int id);
	std::vector<Gesture*> getAllGestures();
private:
	sqlite3 * _db;
	static int callback(void* notUsed, int argc, char** argv, char** azCol);
	static void clearTable();
};
