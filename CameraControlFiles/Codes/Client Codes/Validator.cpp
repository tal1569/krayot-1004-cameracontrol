#include "Validator.h"

/*
Function will check if a password is legal
Input: the password
Output: true or false
*/
bool isPasswordValid(string password)
{
	bool isThereDigit = false;
	bool isThereUpper = false;
	bool isThereLower = false;
	int i = 0;

	if (password.length() < MIN_LENGTH) //Checking if the length of the password is at least 8
	{
		return false;
	}
	if (password.find_first_of(SPACE) != string::npos) //Checking if there are no spaces in the password
	{
		return false;
	}
	
	while ((i < password.length()) && (!isThereDigit)) //Checking if there is a digit in the password
	{
		if (isdigit(password[i]))
		{
			isThereDigit = true;
		}

		i++;
	}

	if (!isThereDigit)
	{
		return false;
	}

	i = 0;

	while ((i < password.length()) && (!isThereUpper)) //Checking if there is a capital letter in the password
	{
		if (isupper(password[i]))
		{
			isThereUpper = true;
		}

		i++;
	}
	if (!isThereUpper)
	{
		return false;
	}

	i = 0;

	while ((i < password.length()) && (!isThereLower)) //Checking if there is a lower letter in the password
	{
		if (islower(password[i]))
		{
			isThereLower = true;
		}

		i++;
	}
	if (!isThereLower)
	{
		return false;
	}

	return true;
}

/*
Function will check if the username is legal
Input: the username
Output: none
*/
bool isUsernameValid(string username)
{
	if (username.length() == ZERO) //Checking if the username has something in it
	{
		return false;
	}
	if (!isalpha(username[ZERO])) //Checking if the username begins with a letter
	{
		return false;
	}
	if (username.find_first_of(SPACE) != string::npos) //Checking if the username has spaces in it
	{
		return false;
	}

	return true;
}

/*
Function will check if the email is valid
Input: the email
Output: true or false
*/
bool isEmailValid(string email)
{
	if (email.find_first_of(' ') != string::npos) //No spaces are alowed
	{
		return false;
	}

	if (isalpha(email[0]) == false) //Email must start with a letter
	{
		return false;
	}

	if (email.find_first_of('@') == string::npos) //Email must contain the char '@'
	{
		return false;
	}

	if (email.find_first_of('@') != email.find_last_of('@')) //Email must contain only 1 '@'
	{
		return false;
	}

	if (email.find_first_of('.') == string::npos) //Email must contain the char '.'
	{
		return false;
	}

	if (email.find_first_of('.') != email.find_last_of('.')) //Email must contain only 1 '.'
	{
		return false;
	}

	if (email.find_first_of('@') > email.find_first_of('.')) //The '@' sould be before the '.'
	{
		return false;
	}

	if ((email.find_first_of('.') - email.find_first_of('@')) == 1) //There sould be text between the '@' and the '.'
	{
		return false;
	}

	if ((email.length() - email.find_first_of('.')) == 1) //The '.' cannot be the last char in the email
	{
		return false;
	}

	return true;
}