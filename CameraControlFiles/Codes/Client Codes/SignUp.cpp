#include "SignUp.h"

/*
Function is the c'tor of the class
Input: the parent
Output: none
*/
SignUp::SignUp(QWidget *parent) : QMainWindow()
{
	this->_ui.setupUi(this);
	this->_prevWindow = parent;
}

/*
Function will handle the click on the "back" button
Input: none
Output: none
*/
void SignUp::backHandle()
{
	this->hide();
	this->_prevWindow->show();
	delete this;
}

/*
Function will handle the click on the "sign up" button
Input: none
Output: none
*/
void SignUp::signUpHandle()
{
	//Send to Server and wait of sucssess or fail
	QString username = this->_ui.Username->text();
	QString password = this->_ui.Password->text();
	QString email = this->_ui.Email->text();

	if (isUsernameValid(username.toStdString()) == false) //Checking if the username is legal
	{		
		QMessageBox* messageBox = new QMessageBox(this);
		messageBox->setText("A username should begin with a letter and should not contain spaces\n");
		messageBox->setWindowTitle("Illegal username");
		messageBox->addButton(QMessageBox::Ok);
		messageBox->setStyleSheet("image:none");
		QIcon icon("://Resources//Resource Files//Sad_Face.png");

		messageBox->setWindowIcon(icon);
		messageBox->show();
		messageBox->exec();
	}
	else
	{
		if (isPasswordValid(password.toStdString()) == false) //Checking if the password is legal
		{			
			QMessageBox* messageBox = new QMessageBox(this);
			messageBox->setText("The length of the password should be at least 8, and it should contain capital letters, small letters and digits with no spaces\n");
			messageBox->setWindowTitle("Illegal password");
			messageBox->addButton(QMessageBox::Ok);
			messageBox->setStyleSheet("image:none");
			QIcon icon("://Resources//Resource Files//Sad_Face.png");

			messageBox->setWindowIcon(icon);
			messageBox->show();
			messageBox->exec();
		}
		else
		{
			if (isEmailValid(email.toStdString()) == false)
			{
				QMessageBox* messageBox = new QMessageBox(this);
				messageBox->setText("Your email is no bueno\n");
				messageBox->setWindowTitle("Illegal email");
				messageBox->addButton(QMessageBox::Ok);
				messageBox->setStyleSheet("image:none");
				QIcon icon("://Resources//Resource Files//Sad_Face.png");

				messageBox->setWindowIcon(icon);
				messageBox->show();
				messageBox->exec();
			}
			else
			{
				try
				{
					this->_client = new Client(0, this);
					vector<string> ipAndPort = readConfigFile();

					this->_client->connect(ipAndPort[0], atoi(ipAndPort[1].c_str()));
					this->_client->startConversation();

					if (this->_client->signup(username.toStdString(), md5(password.toStdString()), email.toStdString()) == SOCKET_ERROR)
					{
						MainWindow* form = new MainWindow();
						QMessageBox* messageBox = new QMessageBox(form);
						messageBox->setText("Server crashed");
						messageBox->setWindowTitle("Can't connect to server");
						messageBox->addButton(QMessageBox::Ok);
						messageBox->setStyleSheet("image:none");
						messageBox->show();

						this->hide();
						form->show();
						delete this;
						return;
					}
				}
				catch (...)
				{
					QMessageBox* messageBox = new QMessageBox(this);
					messageBox->setText("Can't connect to server");
					messageBox->setWindowTitle("Can't connect to server");
					messageBox->addButton(QMessageBox::Ok);
					messageBox->setStyleSheet("image:none");

					QIcon icon("://Resources//Resource Files//Sad_Face.png");

					messageBox->setWindowIcon(icon);
					messageBox->show();
					messageBox->exec();
				}
			}
		}
	}
}

/*
Function will check what is the status of the sign up request
Input: the message from the server
Output: none
*/
void SignUp::signupStatus(std::string msgId)
{
	if (msgId[0] == '0') //Success, even great success even even
	{
		this->_ui.Test->setText("GREAT SUCCESS");
	}
	else if (msgId[0] == '1') //illegal password
	{
		this->_ui.Test->setText("Illegal password");
	}
	else if (msgId[0] == '2') // Username already exists
	{
		this->_ui.Test->setText("Username already exists");
	}
	else if (msgId[0] == '3') // Username is illegal
	{
		this->_ui.Test->setText("Username is illegal");
	}
	else //Other
	{
		this->_ui.Test->setText("There is another reason why you could not sign up");
	}
}