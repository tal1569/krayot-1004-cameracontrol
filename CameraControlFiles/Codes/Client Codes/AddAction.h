#pragma once

#include <QtWidgets/qwidget.h>
#include <QCloseEvent>
#include <QMessageBox>

#include "ui_AddAction.h"
#include "Client.h"
#include "Guest.h"
#include "MainWindow.h"
#include "Header.h"
#include "CameraWindow.h"

class Client;
class Guest;
class Camera;
class EditGesture;
class MainWindow;

class AddAction : public QMainWindow
{
	Q_OBJECT

public:
	AddAction(QWidget *parent, bool onlineMode, Client* client, Guest* guest);
	~AddAction();

	void setPicture(QImage* img);

	void addGestureStatus(bool status);

private:
	Ui::AddActionClass _ui;
	QWidget* _prevWindow;
	Client* _client;
	Guest* _guest;
	bool _onlineMode;	//if true we need to use the client, else use the guest mode.
	Camera* _camera;
	EditGesture* _editForm;
	bool _flag;

	bool _cameraFlag;

private slots:
	void closeEvent(QCloseEvent *event);
	void backHandle();
	void addHandle();
	void startCamera();
};
