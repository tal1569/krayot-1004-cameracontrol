#pragma once

#include <QtWidgets/qwidget.h>
#include "ui_SignUp.h"
#include "Client.h"
#include "MainWindow.h"
#include "Header.h"

class Client;
class MainWindow;


class SignUp : public QMainWindow
{
	Q_OBJECT

public:
	SignUp(QWidget *parent);
	void signupStatus(std::string msgId);

private:
	Ui::SignUpClass _ui;
	QWidget* _prevWindow;
	Client* _client;


private slots:
	void backHandle();
	void signUpHandle();

};

