#include "Gesture.h"

/*
Function is the c'tor of the class
Input: The number of fingers which are raised, which fingers are raised and the action of the gesture
Output: None
*/
Gesture::Gesture(int id, int numOfFingers, bool fingers[NUM_OF_FINGERS], string action)
{
	this->_id = id;
	this->_numOfFingers = numOfFingers;
	
	for (int i = 0; i < NUM_OF_FINGERS; i++)
	{
		this->_fingers[i] = fingers[i];
	}

	this->_action = action;
}

/*
	Function is the c'tor of the class
	Input: The number of fingers which are raised and which fingers are raised.
	Output: None
*/
Gesture::Gesture(int numOfFingers, bool fingers[NUM_OF_FINGERS])
{
	for (int i = 0; i < NUM_OF_FINGERS; i++)
	{
		this->_fingers[i] = fingers[i];
	}
	this->_numOfFingers = numOfFingers;

	this->_action = "";
	this->_id = 0;
}

/*
Function is the d'tor of the class
Input: none
Output: none
*/
Gesture::~Gesture()
{
	///There is nothing to do here
}

/*
Function will compare the gesture to another one
Input: another gesture
Output: true or false
*/
bool Gesture::compare(Gesture other)
{

	if (this->_id != other.getId()) //Checking if the id is the same
	{
		return false;
	}

	if (this->_numOfFingers != other.getNumOfFingers()) //Checking if the same amount of fingers are risen
	{
		return false;
	}

	if (this->_action.compare(other.getAction()) != 0) //Checking if the 2 gestures do the same action
	{
		return false;
	}

	for (int i = 0; i < NUM_OF_FINGERS; i++) //Checking if the same fingers are risen
	{
		if (this->_fingers[i] != other.getFingers()[i])
		{
			return false;
		}
	}

	return true;
}


/*
	Function will compare the gesture to another one by th
	Input: another gesture
	Output: true or false
*/
bool Gesture::compareByFingers(Gesture* other)
{
	if (this->_numOfFingers != other->getNumOfFingers()) //Checking if the same amount of fingers are risen
		return false;

	for (int i = 0; i < NUM_OF_FINGERS; i++) //Checking if the same fingers are risen
		if (this->_fingers[i] != other->getFingers()[i])
			return false;

	return true;
}

/*
Function will return the id of the gesture
Input: none
Output: the id of the gesture
*/
int Gesture::getId()
{
	return this->_id;
}

/*
Function will return the number of fingers risen
Input: none
Output: the amount of fingers risen
*/
int Gesture::getNumOfFingers()
{
	return this->_numOfFingers;
}

/*
Function will return the array which represents the fingers
Input: none
Output: the fingers
*/
bool* Gesture::getFingers()
{
	return this->_fingers;
}

/*
Function will return the action which the gesture needs to do
Input: none
Output: the action
*/
string Gesture::getAction()
{
	return this->_action;
}

/*
Function will return all the data of the gesture as a string
Input: none
Output: all the data of the gesture
*/
string Gesture::toString()
{
	string data = Helper::getPaddedNumber(this->_id, 4); //Adding the id as a 4 digit string to the data
	data = data + Helper::getPaddedNumber(this->_numOfFingers, 1); //Adding the number of fingers that are raised to the data

	for (int i = 0; i < NUM_OF_FINGERS; i++) //Adding the index of the fingers that are raised to the data
	{
		if (this->_fingers[i]) //If the finger is raised it will add the (index + 1) to the message
		{
			data = data + Helper::getPaddedNumber(i + 1, 1);
		}
		else //If not it will add 0 to the message
		{
			data = data + Helper::getPaddedNumber(0, 1);
		}
	}

	data = data + Helper::getPaddedNumber(this->_action.length(), 3); //Adding the length of the action to the data as a 3 digit string
	
	data = data + this->_action; //Adding the action to the data

	return data;
}


/*
Function will set the id of gesture to new id
Input: id to change
Output: none
*/
void Gesture::setId(int id)
{
	this->_id = id;
}

/*
Function will set the action of gesture to new action
Input: action to change
Output: none
*/
void Gesture::setAction(string action)
{
	this->_action = action;
}

/*
	The function return info string to edit form
	Input: none
	Output: info string
*/
string Gesture::getInfo()
{
	string data = std::to_string(this->_id) + '\t' + std::to_string(this->_numOfFingers) + '\t';

	for (int i = 0; i < NUM_OF_FINGERS; i++) //Adding the index of the fingers that are raised to the data
	{
		if (this->_fingers[i]) //If the finger is raised it will add the (index + 1) to the message
		{
			data = data + Helper::getPaddedNumber(i + 1, 1);
		}
		else //If not it will add 0 to the message
		{
			data = data + Helper::getPaddedNumber(0, 1);
		}
	}
	if (this->_action.size() > 3)
	{
		int actionTypeSize = atoi(this->_action.substr(0, 2).c_str());
		string command = this->_action.substr(2);

		if (command.find_first_of('/') != string::npos)
		{
			string temp = command.substr(0, actionTypeSize);

			int lastSlashIndex = command.find_last_of('/');
			command = temp + " - " + command.substr(lastSlashIndex + 1);
		}

		data = data + '\t' + command;
	}
	
	return data;
}

/*
Function will return the index of the fingers
Input: none
Output: the index of the fingers
*/
string Gesture::getFingersIndex()
{
	string fingersIndex = "";
	for (int i = 0; i < 5; i++)
	{
		if (this->_fingers[i])
			fingersIndex = fingersIndex + std::to_string(1 + i);
		else
			fingersIndex = fingersIndex + '0';
	}

	return fingersIndex;
}