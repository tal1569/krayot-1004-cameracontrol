#include "AddAction.h"

/*
Function is the c'tor of the class
Input: the parent, whether or not the user is connected to the server, the client and the guest
Output: none
*/
AddAction::AddAction(QWidget *parent, bool onlineMode, Client* client, Guest* guest)
	: QMainWindow()
{
	_ui.setupUi(this);

	this->_ui.AddButton->setDisabled(true);

	this->_prevWindow = parent;
	this->_client = client;
	this->_guest = guest;
	
	this->_editForm = nullptr;

	try
	{
		//start the camera module
		int cameraIndex = atoi(readConfigFile()[2].c_str());
		this->_camera = new Camera(cameraIndex, this);
	}
	catch (...)
	{
		Menu* form = new Menu(nullptr, this->_onlineMode, this->_client, this->_guest);
		QMessageBox* messageBox = new QMessageBox(form);
		messageBox->setText("Camera Error");
		messageBox->setWindowTitle("Can't open the camera");
		messageBox->addButton(QMessageBox::Ok);
		messageBox->setStyleSheet("image:none");
		messageBox->show();

		this->hide();
		form->show();
		return;
	}

	this->_cameraFlag = true;

	if (onlineMode == true)
		this->_onlineMode = true;	//if true we need to use the client, else use the guest mode.
	else
		this->_onlineMode = false;

}

/*
Function is the d'tor of the function
*/
AddAction::~AddAction()
{
	//We do nothing here
}

/*
Function will handle the push of the "back" button
Input: none
Output: none
*/
void AddAction::backHandle()
{
	this->_camera->disconnectForm();
	this->_cameraFlag = false;

	if (this->_onlineMode == true)
	{
		this->_client->setForm(this->_prevWindow);
	}

	this->hide();
	this->_prevWindow->show();
	delete this;
}

/*
Function will handle the push of the "x" button
Input: none
Output: none
*/
void AddAction::closeEvent(QCloseEvent *event)
{
	QMessageBox* messageBox = new QMessageBox(this);
	messageBox->setText("Are you sure?");
	messageBox->setWindowTitle("Exit");
	messageBox->addButton(QMessageBox::No);
	messageBox->addButton(QMessageBox::Yes);

	messageBox->setStyleSheet("image:none");
	messageBox->show();
	messageBox->exec();

	int resBtn = messageBox->exec(); 

	if (resBtn != (int)(QMessageBox::Yes))
	{
		event->ignore();
	}
	else
	{
		if (this->_onlineMode == true)
			this->_client->signOut();

		this->_camera->disconnectForm();
		cv::destroyAllWindows();

		event->accept();
	}
}

/*
Function will handle the "Add Action" button
Input: none
Output: none
*/
void AddAction::addHandle()
{
	//sent to client/guest to add to db.
	//ask the user if the hand detection is good.
	this->_camera->disconnectForm();
	Gesture* gesture = this->_camera->getCurrectGesture();

	if (gesture != nullptr)	//there are hand
	{
		string fingersIndex = "";
		bool* fingers = gesture->getFingers();

		for (int i = 0; i < 5; i++)
		{
			if (fingers[i] == true)
				fingersIndex = fingersIndex + std::to_string(1 + i);
			else
				fingersIndex = fingersIndex + '0';
		}


		gesture->setAction(" ");
		if (this->_onlineMode == true)
		{
			if(this->_client->addGesture(gesture) == SOCKET_ERROR)
			{
				MainWindow* form = new MainWindow();
				QMessageBox* messageBox = new QMessageBox(form);
				messageBox->setText("Server crashed");
				messageBox->setWindowTitle("Can't connect to server");
				messageBox->addButton(QMessageBox::Ok);
				messageBox->setStyleSheet("image:none");
				messageBox->show();
				
				
				this->_camera->disconnectForm();
				this->_cameraFlag = false;

				if (this->_onlineMode == true)
				{
					this->_client->setForm(this->_prevWindow);
				}

				this->hide();
				form->show();
				delete this;
				return;
			}
		}
		else
		{
			this->_guest->addGesture(gesture);
		}

		this->_editForm = new EditGesture(this, this->_onlineMode, this->_client, this->_guest, gesture);
		this->hide();
		this->_editForm->show();
	}
}

/*
Function will set the picture on the form
Input: the image
Output: none
*/
void AddAction::setPicture(QImage* img)
{
	if (this->_cameraFlag == true)
		this->_ui.image->setPixmap(QPixmap::fromImage(*img));
}

/*
Function will start the camera running
Input: none
Output: none
*/
void AddAction::startCamera()
{
	this->_camera->cameraManager(this->_onlineMode, true, this->_client, this->_guest);
	if (this->_onlineMode == true)
	{
		this->_client->setForm(this);
	}

	this->_ui.StartCameraButton->setEnabled(false);
	this->_ui.AddButton->setEnabled(true);
}

/*
Function will change the status of the gesture
Input: the status
Output: none
*/
void AddAction::addGestureStatus(bool status)
{
	if (status == false)
	{
		this->_editForm->setFlag();
	}
}