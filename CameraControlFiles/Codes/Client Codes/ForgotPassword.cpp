#include "ForgotPassword.h"

/*
Function is the c'tor
Input: the parent
Output: none
*/
ForgotPassword::ForgotPassword(QWidget *parent)
	: QMainWindow()
{
	this->_ui.setupUi(this);
	this->_prevWindow = parent;
	this->_client = nullptr;
}

/*
Function is the d'tor
*/
ForgotPassword::~ForgotPassword()
{
	//We do nothing here
}

/*
Function will send the the "forgot password" message to the server
Input: none
Output: none
*/
void ForgotPassword::sendForgotPassword()
{
	if (this->_ui.username->text() != "")
	{

		try
		{
			this->_client = new Client(0, this);
			this->_client->connect(HOST_IP, HOST_PORT);
			this->_client->startConversation();

			if (this->_client->forgotPassword(this->_ui.username->text().toStdString()) == SOCKET_ERROR)
			{
				MainWindow* form = new MainWindow();
				QMessageBox* messageBox = new QMessageBox(form);
				messageBox->setText("Server crashed");
				messageBox->setWindowTitle("Can't connect to server");
				messageBox->addButton(QMessageBox::Ok);
				messageBox->setStyleSheet("image:none");
				messageBox->show();
				if (this->_client != nullptr)
					this->_client->exitApp();

				this->hide();
				form->show();
				delete this;
				return;
			}
		}
		catch (...)
		{

			QMessageBox* messageBox = new QMessageBox(this);
			messageBox->setText("Can't connect to server");
			messageBox->setWindowTitle("Can't connect to server");
			messageBox->addButton(QMessageBox::Ok);

			messageBox->setStyleSheet("image:none");
			messageBox->show();
			messageBox->exec();
		}
	}
	
}

/*
Function will handle the click on the "back" button
Input: none
Output: none
*/
void ForgotPassword::backHandle()
{
	if (this->_client != nullptr)
		this->_client->exitApp();
	this->hide();
	this->_prevWindow->show();
	delete this;
}

/*
Function will handle the push of the "x" button
Input: none
Output: none
*/
void ForgotPassword::closeEvent(QCloseEvent *event)
{
	QMessageBox* messageBox = new QMessageBox(this);
	messageBox->setText("Are you sure?");
	messageBox->setWindowTitle("Exit");
	messageBox->addButton(QMessageBox::No);
	messageBox->addButton(QMessageBox::Yes);

	messageBox->setStyleSheet("image:none");
	messageBox->show();

	int resBtn = messageBox->exec();

	if (resBtn != (int)(QMessageBox::Yes))
	{
		event->ignore();
	}
	else
	{
		event->accept();
	}
}