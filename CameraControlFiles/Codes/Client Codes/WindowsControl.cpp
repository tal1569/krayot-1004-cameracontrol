#include "WindowsControl.h"

/*
Function will manage the action and execute it's command
Input: the action
Output: none
*/
void WindowsControl::manageAction(std::string action)
{
	int codeSize = atoi(action.substr(0, 2).c_str());
	action = action.substr(2);

	std::string code = action.substr(0, codeSize);

	if (code.compare(LEFT_CLICK) == 0)
	{
		this->leftClick();
	}
	else if (code.compare(RIGHT_CLICK) == 0)
	{
		this->rightClick();
	}
	else if (code.compare(DOUBLE_CLICK) == 0)
	{
		this->doubleClick();
	}
	else if (code.compare(PLAYMP3) == 0)
	{
		action = action.substr(codeSize);
		int pathSize = atoi(action.substr(0, 3).c_str());
		action = action.substr(3);

		std::string path = action.substr(0, pathSize);
		this->playMP3(path);
	}
	else if (code.compare(PLAY_PLAYLIST) == 0)
	{
		action = action.substr(codeSize);
		int pathSize = atoi(action.substr(0, 3).c_str());
		action = action.substr(3);

		std::string path = action.substr(0, pathSize);
		this->playMP3Playlist(path);
	}
	else if (code.compare(OPEN_APP) == 0)
	{
		action = action.substr(codeSize);
		int pathSize = atoi(action.substr(0, 3).c_str());
		action = action.substr(3);

		std::string path = action.substr(0, pathSize);
		std::thread t(&WindowsControl::openApp, this, path);
		t.detach();
	}
	else if (code.compare(INCREASE_VOLUNE) == 0)
	{
		this->increaseVolume();
	}
	else if (code.compare(DECREASE_VOLUME) == 0)
	{
		this->decreaseVolume();
	}
	else if (code.compare(MUTE) == 0)
	{
		this->mute();
	}
	else if (code.compare(UNMUTE) == 0)
	{
		this->unmute();
	}
}

/*
Function will make one left click with the mouse
Input: none
Output: true or false
*/
bool WindowsControl::leftClick()
{
	try
	{
		INPUT    Input = { 0 };
		// left down 
		Input.type = INPUT_MOUSE;
		Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
		::SendInput(1, &Input, sizeof(INPUT));

		// left up
		::ZeroMemory(&Input, sizeof(INPUT));
		Input.type = INPUT_MOUSE;
		Input.mi.dwFlags = MOUSEEVENTF_LEFTUP;
		::SendInput(1, &Input, sizeof(INPUT));

		return true;
	}
	catch (...)
	{
		return false;
	}
}

/*
Function will do a right click
Input: none
Output: true or false
*/
bool WindowsControl::rightClick() 
{
	try
	{
		INPUT    Input = { 0 };
		// right down 
		Input.type = INPUT_MOUSE;
		Input.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
		::SendInput(1, &Input, sizeof(INPUT));

		// right up
		::ZeroMemory(&Input, sizeof(INPUT));
		Input.type = INPUT_MOUSE;
		Input.mi.dwFlags = MOUSEEVENTF_RIGHTUP;
		::SendInput(1, &Input, sizeof(INPUT));

		return true;
	}
	catch (...)
	{
		return false;
	}
}

/*
Function will make a double click
Input: none
Output: true or false
*/
bool WindowsControl::doubleClick()
{
	try
	{
		this->leftClick();
		Sleep(300);
		this->leftClick();

		return true;
	}
	catch (...)
	{
		return false;
	}
}

/*
Function will play a mp3 file
Input: the path
Output: true or false
*/
bool WindowsControl::playMP3(std::string path)
{
	try
	{
		std::string command = "start wmplayer.exe -p " + path;

		system(command.c_str());

		return true;
	}
	catch (...)
	{
		return false;
	}
}

/*
Function will play a playlist
Input: the path of the playlist
Output: true or false
*/
bool WindowsControl::playMP3Playlist(std::string folder)
{
	return this->playMP3(folder);
}

/*
Function will open an app
Input: the path of the file
Output: true or false
*/
bool WindowsControl::openApp(std::string path)
{
	try
	{
		system(path.c_str());

		return true;
	}
	catch (...)
	{
		return false;
	}
}

/*
Function will increase the volume of the computer
Input: none
Output: none
*/
void WindowsControl::increaseVolume()
{
	system("nircmd.exe changesysvolume 1000");
}

/*
Function will decrease the volume of the computer
Input: none
Output: none
*/
void WindowsControl::decreaseVolume()
{
	system("nircmd.exe changesysvolume -1000");
}

/*
Function will mute the computer
Input: none
Output: none
*/
void WindowsControl::mute()
{
	system("nircmd.exe mutesysvolume 1");
}

/*
Function will unmute the computer
Input: none
Output: none
*/
void WindowsControl::unmute()
{
	system("nircmd.exe mutesysvolume 0");
}

/*
	The function move the mouse to the point that the function get
	Input: point to set the mouse to
	Output: none
*/
void WindowsControl::moveMouse(int x, int y)
{
	if(x > 0 && y > 0)
		SetCursorPos(x, y);
}