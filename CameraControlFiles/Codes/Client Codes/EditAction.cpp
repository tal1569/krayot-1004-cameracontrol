#include "EditAction.h"

/*
Function is the c'tor of the class
Input: the parent, whether or not the user is connected to the server, the client and the guest
Output: none
*/
EditAction::EditAction(QWidget *parent, bool onlineMode, Client* client, Guest* guest)
	: QMainWindow()
{
	this->_ui.setupUi(this);

	this->_prevWindow = parent;
	this->_client = client;
	this->_guest = guest;
	this->_onlineMode = onlineMode;	//if true we need to use the client, else use the guest mode.

	
	this->refreshHandle();
}

/*
Function is the d'tor of the function
Input: none
Output: none
*/
EditAction::~EditAction()
{
	//We do nothing here
}

/*
Function will get all the gestures of the user from the database
Input: none
Output: none
*/
void EditAction::getAllGestures()
{
	this->_userGesture.clear();
	//get all the action and show it on the form
	if (this->_onlineMode == true)//client
		this->_userGesture =  this->_client->getAllLocalGestures();
	else
		this->_userGesture =  this->_guest->getAllLocalGesture();		
}

/*
Funcion will handle the push of the "back" button
Input: none
Output: none
*/
void EditAction::backHandle()
{
	this->hide();
	this->_prevWindow->show();
	delete this;
}

/*
Function will handle the push of the "refresh" button
Input: none
Output: none
*/
void EditAction::refreshHandle()
{
	//show all the Gesture that on this->_userGesture.
	
	if (this->_onlineMode == true) //client
	{
		if(this->_client->getAllGestures() == SOCKET_ERROR)
		{
			MainWindow* form = new MainWindow();
			QMessageBox* messageBox = new QMessageBox(form);
			messageBox->setText("Server crashed");
			messageBox->setWindowTitle("Can't connect to server");
			messageBox->addButton(QMessageBox::Ok);
			messageBox->setStyleSheet("image:none");
			messageBox->show();
			
			this->hide();
			form->show();
			delete this;
			return;
		}
	}
	
	this->getAllGestures();
	
	//update the list in the form
	this->updateFormList();
}

/*
Function will handle the push of the "x" button
Input: none
Output: none
*/
void EditAction::closeEvent(QCloseEvent *event)
{
	QMessageBox* messageBox = new QMessageBox(this);
	messageBox->setText("Are you sure?");
	messageBox->setWindowTitle("Exit");
	messageBox->addButton(QMessageBox::No);
	messageBox->addButton(QMessageBox::Yes);

	messageBox->setStyleSheet("image:none");
	messageBox->show();

	int resBtn = messageBox->exec();

	if (resBtn != (int)(QMessageBox::Yes))
	{
		event->ignore();
	}
	else
	{
		if (this->_onlineMode == true)
			this->_client->signOut();

		event->accept();
	}
}

/*
Function will update the list
Input: none
Output: none
*/
void EditAction::updateFormList()
{


	this->_ui.Gestures->clear();

	QString temp = "Id          Fingers Number\tFingers\tAction";
	
	this->_ui.Gestures->addItem(temp);

	this->_ui.Gestures->setCurrentRow(0);

	for (int i = 0; i < this->_userGesture.size(); i++)
	{
		try
		{

			QString temp(this->_userGesture[i]->getInfo().c_str());

			this->_ui.Gestures->addItem(QString::fromStdString(this->_userGesture[i]->getInfo()));

		}
		catch(...)
		{

		}
	}
	this->_ui.Gestures->show();
}


/*
Function will handle the push of the "edit" button
Input: none
Output: none
*/
void EditAction::editHandle()
{
	if (this->_ui.Gestures->currentRow() != 0)		// the first row is the title
	{
		//open form of change action.
		EditGesture* form = new EditGesture(this, this->_onlineMode, this->_client, this->_guest, this->_userGesture[this->_ui.Gestures->currentRow() - 1]);
		form->show();
	}
}

/*
Function will handle the push on the "delete" button
Input: none
Output: none
*/
void EditAction::deleteHandle()
{
	if (this->_ui.Gestures->currentRow() != 0)		// the first row is the title
	{
		//meesage box
		QString message = "Delete " + QString(this->_ui.Gestures->currentItem()->text()) + " ?";

		QMessageBox* messageBox = new QMessageBox(this);
		messageBox->setText(message);
		messageBox->setWindowTitle("Delete Gesture?");
		messageBox->addButton(QMessageBox::No);
		messageBox->addButton(QMessageBox::Yes);

		messageBox->setStyleSheet("image:none");
		messageBox->show();

		int resBtn = messageBox->exec();

		if (resBtn == (int)(QMessageBox::Yes))
		{
			//get the id of gesture
			int id = this->_userGesture[this->_ui.Gestures->currentRow() - 1]->getId();

			//delete Gesture
			if (this->_onlineMode == true)	//client mode
			{
				if (this->_client->deleteGesutre(id) == SOCKET_ERROR)
				{
					MainWindow* form = new MainWindow();
					QMessageBox* messageBox = new QMessageBox(form);
					messageBox->setText("Server crashed");
					messageBox->setWindowTitle("Can't connect to server");
					messageBox->addButton(QMessageBox::Ok);
					messageBox->setStyleSheet("image:none");
					messageBox->show();

					this->hide();
					form->show();
					delete this;
					return;
				}
			}
			else // guest mode
				this->_guest->deleteGesture(id);
		}

		//refresh
		this->getAllGestures();
		this->updateFormList();
	}
}