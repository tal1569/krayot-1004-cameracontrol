#pragma once

#pragma comment (lib, "ws2_32.lib")
#include <Windows.h>
#include <string>
#include <exception>
#include <iostream>
#include <thread>
#include <condition_variable>
#include <queue>

#include "ClassIncludes.h"
#define CAMERA_INDEX 1

class Camera;

class Client
{
public:
	Client(int cameraIndex, QWidget* form);
	~Client();
	void connect(std::string serverIP, int port);
	void startConversation();

	void listening();

	int signin(string username, string password);
	int signup(std::string username, std::string password, std::string email);
	int signOut();
	int forgotPassword(string username);

	int exitApp();
	int getAllGestures();

	int addGesture(Gesture* gesture);
	int deleteGesutre(int gestureId);
	int editAction(string action, string fingers);

	void exeAction(int id);
	void compareGesture(Gesture* gesture);

	std::vector<Gesture*> getAllLocalGestures();
	void setForm(QWidget* form);

private:
	SOCKET _clientSocket;
	bool _exit;
	bool _signedIn;
	int _cameraIndex;
	string _username;


	QWidget* _form;

	std::vector<Gesture*> _userGestures;
};