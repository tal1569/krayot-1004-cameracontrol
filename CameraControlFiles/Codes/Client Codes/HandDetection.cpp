#include "HandDetection.h"

#define LIMIT_ANGLE_SUP 60
#define LIMIT_ANGLE_INF 5
#define BOUNDING_RECT_FINGER_SIZE_SCALING 0.3
#define BOUNDING_RECT_NEIGHBOR_DISTANCE_SCALING 0.05
#define MAX_COUNT_FRMAE 15
#define MAX_TIMER_COUNTER 1

/*
	The c'tor of hand detection.
*/
HandDetection::HandDetection()
{
	std::vector<cv::Point> temp(5);
	this->_fingerPos = temp;
	this->_message = "";
	this->_timerCounter = 0;
	this->_countFrame = 0;
	this->_frameSave = new std::stack<std::vector<cv::Point>>();
}

/*
	The c'tor of hand detection.
*/
HandDetection::~HandDetection()
{

}

/*
	The function set the binary image.
	Input: binaryImage to set.
	Output: none
*/
void HandDetection::setBinaryImage(cv::Mat binaryImage)
{
	resize(binaryImage, this->_binaryImage, cv::Size(binaryImage.cols / 2, binaryImage.rows / 2));
}

/*
	The main fanction of hand detection
	Input:
	Output:
*/
Gesture* HandDetection::detectHand(cv::Mat bwFrame, bool onlineMode, Client* client, Guest* guest, bool addAction,QWidget* form)
{
	bool gestureFlag = false;
	Gesture* gesture = nullptr;

	setBinaryImage(bwFrame);

	cv::Mat frame;
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;

	//mprphology
	cv::Mat kernel = cv::Mat::ones(5, 5, CV_32F);
	cv::morphologyEx(this->_binaryImage, this->_binaryImage, cv::MORPH_OPEN, kernel);

	GaussianBlur(this->_binaryImage, this->_binaryImage, cv::Size(3, 3), 0);

	cvtColor(this->_binaryImage, frame, cv::COLOR_BGR2GRAY);
	cv::findContours(frame, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_NONE, cv::Point(0, 0));

	// we need at least one contour to work
	if (contours.size() <= 0)
	{
		///imshow("hand detection", this->_binaryImage);
		return NULL;
	}

	// find the biggest contour (let's suppose it's our hand)
	int biggest_contour_index = -1;
	double biggest_area = 0.0;

	for (int i = 0; i < contours.size(); i++) {
		double area = contourArea(contours[i], false);
		if (area > biggest_area) {
			biggest_area = area;
			biggest_contour_index = i;
		}
	}

	//we draw the bigest contours that we found
	rectangle(this->_binaryImage, cv::Point(0, 0), cv::Point(this->_binaryImage.cols - 1, this->_binaryImage.rows - 1), cv::Scalar(0, 0, 0), cv::FILLED);
	drawContours(this->_binaryImage, contours, biggest_contour_index, cv::Scalar(0, 0, 255), 1, 8);

	minEnclosingCircle(contours[biggest_contour_index], this->_minCircle, this->_minRadius);
	circle(this->_binaryImage, this->_minCircle, (int)(this->_minRadius), cv::Scalar(0, 255, 0), 2, 2);

	//check if the min circle has found
	if (this->_minCircle.x < 0 || this->_minCircle.x > this->_binaryImage.cols || this->_minCircle.y < 0 || this->_minCircle.y > this->_binaryImage.rows || this->_minRadius <= 0)
	{
		///imshow("hand detection", this->_binaryImage);
		return NULL;
	}
	
	if (addAction == false && this->_countFrame %2 == 0)
	{
	WindowsControl wc;
	wc.moveMouse((int)(((this->_minCircle.x - this->_minRadius) / (this->_binaryImage.cols - (2 * this->_minRadius))) * 1366), (int)(((this->_minCircle.y - this->_minRadius) / (this->_binaryImage.rows - (2 * this->_minRadius))) * 768));
	}
	cv::Point startPoint((int)(this->_minCircle.x - this->_minRadius), (int)(this->_minCircle.y - this->_minRadius));
	cv::Point endPoint((int)(this->_minCircle.x + this->_minRadius), (int)(this->_minCircle.y + this->_minRadius));

	//check if the hand is found
	if ((startPoint.x < 0 || startPoint.y < 0 || endPoint.x < 0 || endPoint.y < 0) || (startPoint.x > this->_binaryImage.cols || startPoint.y > this->_binaryImage.rows || endPoint.x > this->_binaryImage.cols || endPoint.y > this->_binaryImage.rows) || (endPoint.x < startPoint.x || endPoint.y < startPoint.y))
	{
		///imshow("hand detection", this->_binaryImage);
		return NULL;
	}

	//else the hand has found
	double dist, radius = -1;
	cv::Point center(-1, -1);
	for (int i = startPoint.x; i < endPoint.x; i = i + 4)
	{
		for (int j = startPoint.y; j < endPoint.y; j = j + 4)
		{

			dist = pointPolygonTest(contours[biggest_contour_index], cv::Point(i, j), true);
			if (dist > radius)
			{
				radius = dist;
				center = cv::Point(i, j);
			}
		}
	}


	//check if the circle has found
	if (radius == -1 || center.x == -1 || center.y == -1)
	{
		///imshow("hand detection", this->_binaryImage);
		return NULL;
	}

	//else the max circle has found
	this->_maxRadius = (float)radius;
	this->_maxCircle = center;

	//convex hull
	std::vector<cv::Point> hull_points;
	std::vector<int> hull_ints;

	// for drawing the convex hull and for finding the bounding rectangle
	convexHull(cv::Mat(contours[biggest_contour_index]), hull_points, true);

	// for finding the defects
	convexHull(cv::Mat(contours[biggest_contour_index]), hull_ints, false);

	// we need at least 3 points to find the defects
	std::vector<cv::Vec4i> defects;
	if (hull_ints.size() > 3)
		convexityDefects(cv::Mat(contours[biggest_contour_index]), hull_ints, defects);
	else
		return NULL;

	// we separate the defects keeping only the ones of intrest
	std::vector<cv::Point> start_points;
	std::vector<cv::Point> far_points;

	for (int i = 0; i < defects.size(); i++) {
		start_points.push_back(contours[biggest_contour_index][defects[i].val[0]]);

		// filtering the far point based on the distance from the center of the bounding rectangle
		if (findPointsDistance(contours[biggest_contour_index][defects[i].val[2]], this->_maxCircle) < this->_maxRadius * 2)
			far_points.push_back(contours[biggest_contour_index][defects[i].val[2]]);
	}

	std::vector<cv::Point> filtered_start_points = compactOnNeighborhoodMedian(start_points, this->_minRadius * 2 * BOUNDING_RECT_NEIGHBOR_DISTANCE_SCALING);
	std::vector<cv::Point> filtered_far_points = compactOnNeighborhoodMedian(far_points, this->_minRadius * 2 * BOUNDING_RECT_NEIGHBOR_DISTANCE_SCALING);

	std::vector<cv::Point> filtered_finger_points;

	std::vector<cv::Point> finger_points;
	if (filtered_far_points.size() > 1)
	{

		for (int i = 0; i < filtered_start_points.size(); i++) {
			std::vector<cv::Point> closest_points = findClosestOnX(filtered_far_points, filtered_start_points[i]);

			if (isFinger(closest_points[0], filtered_start_points[i], closest_points[1], LIMIT_ANGLE_INF, LIMIT_ANGLE_SUP, this->_maxCircle, this->_minRadius * 2 * BOUNDING_RECT_FINGER_SIZE_SCALING))
				finger_points.push_back(filtered_start_points[i]);
		}


		if (finger_points.size() > 0) {

			std::vector<cv::Point> vecTemp;
			//we remove all fingers that below: < hand center - (max radius / 2) >
			for (int i = 0; i < finger_points.size(); i++)
			{
				if (finger_points[i].y < this->_minCircle.y)
					vecTemp.push_back(finger_points[i]);
			}

			// we have at most five fingers usually :)
			while (vecTemp.size() > 5)
				vecTemp.pop_back();

			if (vecTemp.size() > 0)
			{
				// filter out the points too close to each other
				for (int i = 0; i < vecTemp.size() - 1; i++)
				{
					if (findPointsDistanceOnX(vecTemp[i], vecTemp[i + 1]) > this->_minRadius * 2 * BOUNDING_RECT_NEIGHBOR_DISTANCE_SCALING * 1.5)
						filtered_finger_points.push_back(vecTemp[i]);
				}


				if (finger_points.size() > 2)
				{
					if (findPointsDistanceOnX(finger_points[0], finger_points[finger_points.size() - 1]) > this->_minRadius * 2 * BOUNDING_RECT_NEIGHBOR_DISTANCE_SCALING * 1.5)
						filtered_finger_points.push_back(finger_points[finger_points.size() - 1]);
				}
				else
					filtered_finger_points.push_back(finger_points[finger_points.size() - 1]);


				if (filtered_finger_points.size() == 5)
				{
					this->_frameSave->push(filtered_finger_points);


					if (this->_frameSave->size() == 5)
					{
						//save the last frame fingers for compare to the others
						std::vector<cv::Point> flag = this->_frameSave->top();
						this->_frameSave->pop();


						//save the fingers on array.	
						for (int i = 0; i < 5; i++)
						{
							this->_fingerPos[i] = filtered_finger_points[i];
							cv::circle(this->_binaryImage, this->_fingerPos[i], this->_maxRadius * 0.6, cv::Scalar(100, 100, 100));
						}
					}


				}
				//WindowsControl wc;
				//wc.moveMouse((int)((this->_minCircle.x / (this->_binaryImage.cols) )*1366), (int)((this->_minCircle.y / (this->_binaryImage.rows))* 768));


				if (this->_countFrame == MAX_COUNT_FRMAE)
				{
					this->_countFrame = 0;
					
					int fingerNum = 0;
					bool* fingerPos = isFingerGone(filtered_finger_points, this->_fingerPos);
					this->_message = "";

					for (int i = 0; i < 5; i++)
					{
						if (fingerPos[i] == true)
						{
							this->_message = this->_message + '1';
							fingerNum++;
						}
						else
							this->_message = this->_message + '0';
					}

					if (this->_timerCounter >= MAX_TIMER_COUNTER)
					{
						this->_timerCounter = 0;
						//we found Gesture
						gesture = new Gesture(fingerNum, fingerPos);
						if (addAction == false)
						{
							if (onlineMode)	//Client Mode
								client->compareGesture(gesture);
							else			//Guest Mode
								guest->compareGesture(gesture);
						}
						gestureFlag = true;
					}
					else
						this->_timerCounter++;
				}
				else
					this->_countFrame++;

				putText(this->_binaryImage, this->_message, cv::Point(50, 50), 1, 5, cv::Scalar(0, 0, 255));
			}
		}
	}

	//draw the info from hand detection

	drawVectorPoints(this->_binaryImage, filtered_start_points, cv::Scalar(255, 0, 0), false);
	drawVectorPoints(this->_binaryImage, filtered_far_points, cv::Scalar(0, 0, 255), false);
	drawVectorPoints(this->_binaryImage, filtered_finger_points, cv::Scalar(180, 105, 255), true);

	cv::Mat temp;
	resize(this->_binaryImage, temp, cv::Size(this->_binaryImage.cols * 2, this->_binaryImage.rows * 2));

	///imshow("hand detection", this->_binaryImage);
	///imshow("temp", temp);

	setImageOnForm(temp,form);

	cv::waitKey(5);

	if (gestureFlag)
		return gesture;
	return NULL;
}

/*
	The fanction draw the points on the image.
	Input: std::vector of points,image,color
	Outut: none.
*/
void HandDetection::drawVectorPoints(cv::Mat image, std::vector<cv::Point> points, cv::Scalar color, bool with_numbers) {
	for (int i = 0; i < points.size(); i++) {
		circle(image, points[i], 5, color, 2, 8);
		if (with_numbers)
			putText(image, std::to_string(i), points[i], cv::FONT_HERSHEY_PLAIN, 3, color);
	}
}

/*
	The function fint the distance between 2 point
	Input: 2 points
	Output: distance between the points
*/
float HandDetection::findPointsDistance(cv::Point2f first, cv::Point2f second)
{
	return sqrtf((second.x - first.x) * (second.x - first.x) + (second.y - first.y) * (second.y - first.y));
}

/*
	The fanction remove the points that too close each other and do the median of that points.
	Input: std::vector of points and max neighbor distance.
	Output: std::vector of points after filter the points.
*/
std::vector<cv::Point> HandDetection::compactOnNeighborhoodMedian(std::vector<cv::Point> points, double max_neighbor_distance)
{
	std::vector<cv::Point> median_points;

	if (points.size() == 0)
		return median_points;

	if (max_neighbor_distance <= 0)
		return median_points;

	// we start with the first point
	cv::Point reference = points[0];
	cv::Point median = points[0];

	for (int i = 1; i < points.size(); i++) {
		if (findPointsDistance(reference, points[i]) > max_neighbor_distance) {

			// the point is not in range, we save the median
			median_points.push_back(median);

			// we swap the reference
			reference = points[i];
			median = points[i];
		}
		else
			median = (points[i] + median) / 2;
	}

	// last median
	median_points.push_back(median);

	return median_points;
}

/*
	The fanction find the angle between 3 points ( abc Angle ).
	Input: 3 points.
	Output: the abc Angle.
*/
double HandDetection::findAngle(cv::Point a, cv::Point b, cv::Point c)
{
	double ab = findPointsDistance(a, b);
	double bc = findPointsDistance(b, c);
	double ac = findPointsDistance(a, c);
	return acos((ab * ab + bc * bc - ac * ac) / (2 * ab * bc)) * 180 / CV_PI;
}

/*
	The fanction find the 2 closes point in Point's std::vector from the point that fanction get
	Input: std::vector of point and index Point.
	Output: the 2 closes points.
*/
std::vector<cv::Point> HandDetection::findClosestOnX(std::vector<cv::Point> points, cv::Point pivot)
{
	std::vector<cv::Point> to_return(2);

	if (points.size() == 0)
		return to_return;

	double distance_x_1 = DBL_MAX;
	double distance_1 = DBL_MAX;
	double distance_x_2 = DBL_MAX;
	double distance_2 = DBL_MAX;
	int index_found = 0;

	for (int i = 0; i < points.size(); i++) {
		double distance_x = findPointsDistanceOnX(pivot, points[i]);
		double distance = findPointsDistance(pivot, points[i]);

		if (distance_x < distance_x_1 && distance_x != 0 && distance <= distance_1) {
			distance_x_1 = distance_x;
			distance_1 = distance;
			index_found = i;
		}
	}

	to_return[0] = points[index_found];

	for (int i = 0; i < points.size(); i++) {
		double distance_x = findPointsDistanceOnX(pivot, points[i]);
		double distance = findPointsDistance(pivot, points[i]);

		if (distance_x < distance_x_2 && distance_x != 0 && distance <= distance_2 && distance_x != distance_x_1) {
			distance_x_2 = distance_x;
			distance_2 = distance;
			index_found = i;
		}
	}

	to_return[1] = points[index_found];

	return to_return;
}

/*
	The fanction get the distance between two points only on the x.
	Input: 2 points.
	Output: the x distance between the points.
*/
double HandDetection::findPointsDistanceOnX(cv::Point a, cv::Point b)
{
	double to_return = 0.0;

	if (a.x > b.x)
		to_return = a.x - b.x;
	else
		to_return = b.x - a.x;

	return to_return;
}

/*
	The fanction get 3 points and check if it is finger
	Input: 3 points to check if it is finger.
	Output: false if it not a finger else true.
*/
bool HandDetection::isFinger(cv::Point a, cv::Point b, cv::Point c, double limit_angle_inf, double limit_angle_sup, cv::Point palm_center, double min_distance_from_palm)
{
	double angle = findAngle(a, b, c);
	if (angle > limit_angle_sup || angle < limit_angle_inf)
		return false;

	// the finger point sohould not be under the two far points			//diss a-o and c-o < b-o
	int delta_1 = findPointsDistance(a, palm_center);					//b.y - a.y;
	int delta_2 = findPointsDistance(b, palm_center);					//b.y - c.y;
	int delta_3 = findPointsDistance(c, palm_center);
	//if (delta_y_1 > 0 && delta_y_2 > 0)
	if (delta_1 > delta_2 || delta_3 > delta_2)
		return false;

	// the two far points should not be both under the center of the hand		
	int delta_y_3 = (palm_center.y + this->_maxRadius * 0.5) - a.y;
	int delta_y_4 = (palm_center.y + this->_maxRadius * 0.5) - c.y;
	int delta_y_5 = (palm_center.y + this->_maxRadius * 0.5) - b.y;
	if (delta_y_3 < 0 || delta_y_4 < 0 || delta_y_5 < 0)
		return false;




	double distance_from_palm = findPointsDistance(b, palm_center);
	if (distance_from_palm < min_distance_from_palm)
		return false;

	// this should be the case when no fingers are up
	double distance_from_palm_far_1 = findPointsDistance(a, palm_center);
	double distance_from_palm_far_2 = findPointsDistance(c, palm_center);
	if (distance_from_palm_far_1 < min_distance_from_palm / 4 || distance_from_palm_far_2 < min_distance_from_palm / 4)
		return false;

	return true;
}


/*
	The fanction check if the points in the same circle with 0.1 * maxRadius
	Input: points to check and flag that it is the hand with 5 fingers ( flag size must be 5 )
	Output: bool array with size of 5, every index it is a finger.
*/
bool* HandDetection::isFingerGone(std::vector<cv::Point> first, std::vector<cv::Point> flag)
{
	bool* returnVal = new bool[5];

	for (int i = 0; i < 5; i++)
		returnVal[i] = false;

	for (int i = 0; i < first.size(); i++)
	{
		cv::Point finger(this->_maxCircle.x - first[i].x, this->_maxCircle.y - first[i].y);

		for (int j = 0; j < flag.size(); j++)
		{
			cv::Point flagPoint(this->_maxCircle.x - flag[j].x, this->_maxCircle.y - flag[j].y);
			if (comparePoint(finger, flagPoint))
				returnVal[4 - j] = true;
		}
	}

	return returnVal;
}

/*
	The function compare two points
	Input: two points
	Output: True if two points is close else False
*/
bool HandDetection::comparePoint(cv::Point first, cv::Point flag)
{
	if (first.x < flag.x + this->_maxRadius * 0.6 && first.x > flag.x - this->_maxRadius * 0.6 && first.y < flag.y + this->_maxRadius * 0.6 && first.y > flag.y - this->_maxRadius * 0.6)
		return true;
	return false;
}