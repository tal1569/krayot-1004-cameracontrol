#pragma once

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <string>
#include <math.h>

#include "CameraWindow.h"
#include "InternalDB.h"
#include "HandDetection.h"
#include "Client.h"
#include "Gesture.h"
#include "Guest.h"

#define KEY_FRAME 40

class HandDetection;
class Guest;
class Client;

class Camera
{
	private:
		//private varibels
		cv::Mat _backgroundImage;

		cv::Mat _originalFrame;
		cv::Mat _YCrCbFrame;
		cv::Mat _bwHandFrame;


		cv::VideoCapture cap;

		string _faceCascadeName = "haarcascade_frontalface_alt.xml";
		cv::CascadeClassifier _faceCascade;
		cv::Rect _face;
		bool _isFace;
		int _keyFrame;

		HandDetection* _handDetection;

		QWidget* _cameraForm;
		bool _formFlag;

		Gesture* _detectionGesture;

		//private functions
		cv::Mat skinExtraction(cv::Mat frame);
		void faceDetection();
		cv::Mat faceRemove(cv::Mat frame);
		cv::Mat backgroundSubstraction(cv::Mat frame);
		cv::Mat morphology(cv::Mat frame);
		cv::Mat maskFrame(cv::Mat bwFrame);
		cv::Mat maxRgbFilter(cv::Mat input);

		

	public:
		Camera(int cameraIndex, QWidget* form);
		~Camera();

		void releaseCamera();

		Gesture* cameraManager(bool onlineMode,bool addAction, Client* client, Guest* guest);

		void setBackgroundImage(cv::Mat backgroundImage);

		void disconnectForm();

		Gesture* getCurrectGesture();

		
};

void setImageOnForm(cv::Mat frame,QWidget* form);