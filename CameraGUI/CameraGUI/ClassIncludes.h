#pragma once

#include "Camera.h"
#include "Client.h"
#include "dirent.h"
#include "Gesture.h"
#include "Guest.h"
#include "HandDetection.h"
#include "Helper.h"
#include "InternalDB.h"
#include "MainWindow.h"
#include "Protocol.h"
#include "SignUp.h"
#include "SignIn.h"
#include "sqlite3.h"
#include "WindowsControl.h"
#include "WSAInitializer.h"


class Camera;
class Client;
class dirent;
class Gesture;
class Guest;
class HandDetection;
class Helper;
class InternalDB;
class MainWindow;
class SignIn;
class SignUp;
class WindowsControl;
class WSAInitislizer;