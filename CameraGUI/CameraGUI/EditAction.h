#pragma once

#include <QtWidgets/qwidget.h>
#include <QCloseEvent>
#include <QMessageBox>

#include "ui_EditAction.h"
#include "Client.h"
#include "Guest.h"
#include "MainWindow.h"
#include "Header.h"
#include "CameraWindow.h"
#include "EditGesture.h"

class Client;
class MainWindow;
class Guest;

class EditAction : public QMainWindow
{
	Q_OBJECT

public:
	EditAction(QWidget *parent, bool onlineMode, Client* client, Guest* guest);
	~EditAction();

	void getAllGestures();

private:
	Ui::EditActionClass _ui;
	bool _onlineMode;
	Client* _client;
	Guest* _guest;
	QWidget* _prevWindow;
	std::vector<Gesture*> _userGesture;

	void updateFormList();

private slots:
	void closeEvent(QCloseEvent *event);
	void backHandle();
	void refreshHandle();
	void editHandle();
	void deleteHandle();
};

