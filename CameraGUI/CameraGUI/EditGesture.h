#pragma once

#include <QtWidgets/qwidget.h>
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>

#include "ui_EditGesture.h"
#include "Client.h"
#include "Guest.h"
#include "MainWindow.h"
#include "Header.h"
#include "CameraWindow.h"

class Client;
class Guest;
class Camera;
class MainWindow;

class EditGesture : public QMainWindow
{
	Q_OBJECT

public:
	EditGesture(QWidget *parent, bool onlineMode, Client* client, Guest* guest,Gesture* gesture);
	~EditGesture();

	void setFlag();

private:
	Ui::EditGestureClass _ui;
	QWidget* _prevWindow;
	Client* _client;
	Guest* _guest;
	bool _onlineMode;	//if true we need to use the client, else use the guest mode.
	Gesture* _gesture;

	QString _filename;

	bool _flag;

private slots:
	void closeEvent(QCloseEvent *event);
	void backHandle();
	void updateGestureAction();
	void listMarked();
	void browseHandle();
};
