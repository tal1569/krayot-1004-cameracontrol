#pragma once

#include <QtWidgets/qwidget.h>
#include <QCloseEvent>
#include <QMessageBox>

#include "ui_Menu.h"
#include "Client.h"
#include "Guest.h"
#include "MainWindow.h"
#include "Header.h"
#include "CameraWindow.h"
#include "EditAction.h"
#include "AddAction.h"

class Client;
class MainWindow;
class Guest;
class EditAction;
class AddAction;

class Menu : public QMainWindow
{
	Q_OBJECT

public:
	Menu(QWidget *parent,bool onlineMode,Client* client,Guest* guest);

private:
	Ui::MenuClass _ui;
	QWidget* _prevWindow;
	Client* _client;
	Guest* _guest;
	bool _onlineMode;	//if true we need to use the client, else use the guest mode.


private slots:
	void signOutHandle();
	void closeEvent(QCloseEvent *event);
	void addActionHandle();
	void editActionHandle();
	void runCameraHandle();
};

