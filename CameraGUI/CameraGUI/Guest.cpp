#include "Guest.h"


/*
	The c'tor of Guest
*/
Guest::Guest(int cameraIndex)
{
	this->_cameraIndex = cameraIndex;
	this->_db = new InternalDB();
	this->_userGestures = this->_db->getAllGestures();
}

/*
	The d'tor of Guest
*/
Guest::~Guest()
{
}

/*
	The function adds new Gesture.
	Input: Gesture to add.
	Output: none.
*/
void Guest::addGesture(Gesture* gesture)
{
	this->_userGestures.push_back(gesture);
	this->_db->addGesture(gesture->getNumOfFingers(), gesture->getFingersIndex(), gesture->getAction());
}

/*
Function will compare a gesture given to the gestures of the guest
Input: a gesture
Output: none
*/
void Guest::compareGesture(Gesture* gesture)
{
	WindowsControl wc;

	for (int i = 0; i < this->_userGestures.size(); i++)
	{
		if (this->_userGestures[i]->compareByFingers(gesture)) //Checking if we found a match
		{
			wc.manageAction(this->_userGestures[i]->getAction()); //Calling for the execution of the action
		}
	}
}

/*
	The function update the camera index
	Input: new camera index
	Output: none
*/
void Guest::updateCameraIndex(int cameraIndex)
{
	this->_cameraIndex = cameraIndex;
}

/*
Function will return all the gestures of the user
Input: none
Output: the gestures of the user
*/
std::vector<Gesture*> Guest::getAllLocalGesture()
{
	return this->_userGestures;
}

/*
Function will delete the gesture
Input: the gesture id
Output: none
*/
void Guest::deleteGesture(int gestureId)
{
	gestureId = this->_db->deleteGesture(gestureId);

	for (int i = 0; i < this->_userGestures.size(); i++)
	{
		if (gestureId == this->_userGestures[i]->getId())
		{
			this->_userGestures.erase(this->_userGestures.begin() + i);
		}
	}
}

/*
Function will edit the gesture of the guest
Input: the action and the fingers
Output: none
*/
void Guest::editGesture(string action, string fingers)
{
	this->_db->editGestureAction(fingers, action);
	this->_userGestures = this->_db->getAllGestures();
}