#include "InternalDB.h"

std::unordered_map<string, std::vector<string>> results;

/*
Function is the c'tor of the InternalDB
Input: none
Output: none
*/
InternalDB::InternalDB()
{
	char *zErrMsg = 0;
	int rc;
	rc = sqlite3_open(DATABASE_NAME, &this->_db);
	if (rc)
	{
		throw("Can't open the datebase");
	}

	rc = sqlite3_exec(this->_db, "CREATE TABLE t_gestures(gesture_id integer primary key autoincrement not null, numOfFingers integer not null, fingers text not null, action text not null)", callback, 0, &zErrMsg);
}

/*
Function is the d'tor of the InternalDB
Input: none
Output: none
*/
InternalDB::~InternalDB()
{
	sqlite3_close(this->_db);
}

/*
Function is a support function
*/
int InternalDB::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			std::pair<string, std::vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

/*
Function is a support function
*/
void InternalDB::clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

/*
Function will check if a gesture exists based on its id
Input: the id
Output: true or false
*/
bool InternalDB::doesGestureExist(int id)
{
	int rc;
	char *zErrMsg = 0;
	bool result;
	string message = "select * from t_gestures where gesture_id = ";
	message = message + "\'" + std::to_string(id) + "\'";
	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //Checking if the gesture exists or not
	{
		result = false;
	}
	else
	{
		result = true;
	}

	this->clearTable();

	return result;
}

/*
Function will recieve the path of the file which represents the gesture and will check if the gesture exists in the DB
Input: the path
Output: true or false
*/
bool InternalDB::doesGestureExist(string path)
{
	int rc;
	char *zErrMsg = 0;
	bool result;
	string message = "select * from t_gestures where path = ";
	message = message + "\'" + path + "\'";
	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //Checking if the gesture exists or not
	{
		result = false;
	}
	else
	{
		result = true;
	}

	this->clearTable();

	return result;
}

/*
Function will check if the gesture exists or not
Input: the path, the number of fingers, the fingers, the action
Output: true or false
*/
bool InternalDB::doesGestureExist(int numOfFingers, string fingers, string action)
{
	int rc;
	char *zErrMsg = 0;

	string message = "select * from t_gestures where ";
	message = message + "numOfFingers = " + std::to_string(numOfFingers) + " and ";
	message = message + "fingers = \'" + fingers + "\' and ";
	message = message + "action = \'" + action + "\'";

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //The gesture does not exist in the database
	{
		return false;
	}

	this->clearTable();
	return true;
}

/*
Function will add a gesture to the database
Input: the path of the file which represents the gesture and the number of fingers lifted in the photo
Output: the id of the added gesture
*/
bool InternalDB::addGesture(int numOfFingers, string fingers, string action)
{
	int rc;
	char *zErrMsg = 0;

	if (!this->doesGestureExist(numOfFingers, fingers, action))
	{
		string message = "insert into t_gestures(numOfFingers, fingers, action) values(";
		message = message + std::to_string(numOfFingers) + ", \'" + fingers + "\', " + "\'" + action + "\')";

		try
		{
			rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);
			this->clearTable();
			return true;
		}
		catch (...)
		{
			return false;
		}
	}

	return false;
}

/*
Function will delete a gesture from the database
Input: the path of the gesture
Output: the id of the deleted gesture
*/
int InternalDB::deleteGesture(int gestureId)
{
	int rc;
	char *zErrMsg = 0;

	int id;
	string getId = "select gesture_id from t_gestures where gesture_id = " + std::to_string(gestureId);
	string message = "delete from t_gestures where gesture_id = " + std::to_string(gestureId);

	try
	{
		rc = sqlite3_exec(this->_db, getId.c_str(), callback, 0, &zErrMsg);
		if (results.begin() == results.end())
		{
			this->clearTable();
			return 0;
		}

		id = atoi(results.begin()->second[0].c_str());

		this->clearTable();

		rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

		this->clearTable();
		return id;
	}
	catch (...)
	{
		return 0;
	}
}

/*
Function will return all the gestures that are stored in the database
Input: none
Output: all the gestures
*/
std::vector<Gesture*> InternalDB::getAllGestures()
{
	int rc;
	char *zErrMsg = 0;
	string message = "select * from t_gestures";
	std::vector<Gesture*> gestures;
	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //We return an empty vector because there are no gestures for the user
	{
		this->clearTable();
		return gestures;
	}
	else
	{
		auto findId = results.find("gesture_id");
		auto findNumOfFingers = results.find("numOfFingers");
		auto findFingers = results.find("fingers");
		auto findAction = results.find("action");

		for (int i = 0; i < findId->second.size(); i++)
		{
			int id = atoi(findId->second[i].c_str());
			int numOfFingers = atoi(findNumOfFingers->second[i].c_str());
			string fingers = findFingers->second[i];
			bool raisedFingers[NUM_OF_FINGERS];

			for (int j = 0; j < NUM_OF_FINGERS; j++)
			{
				if (fingers[j] != '0')
				{
					raisedFingers[j] = true;
				}
				else
				{
					raisedFingers[j] = false;
				}
			}

			string action = findAction->second[i];

			Gesture* gesture = new Gesture(id, numOfFingers, raisedFingers, action);

			gestures.push_back(gesture);
		}

		this->clearTable();

		return gestures;
	}
}

/*
Function will return a gesture based on its id
Input: the id
Output: the gesture
*/
std::vector<string> InternalDB::getGestureById(int id)
{
	int rc;
	char *zErrMsg = 0;
	std::vector<string> gesture;
	string message = "select * from t_gestures where gesture_id = " + std::to_string(id);

	if (doesGestureExist(id)) //Checking if the gesture exists
	{
		rc = sqlite3_exec(this->_db, message.c_str(), callback, NULL, &zErrMsg);
		gesture.push_back(std::to_string(id));
		gesture.push_back(results.find("numOfFingers")->second[0]);
		gesture.push_back(results.find("fingers")->second[0]);
		gesture.push_back(results.find("action")->second[0]);

		this->clearTable();
	}
	return gesture;
}

/*
Function will send the id of the last gesture in the database
Input: none
Output: the id of the last gesture
*/
int InternalDB::getLastGestureId()
{
	int rc;
	char *zErrMsg = 0;

	string message = "select gesture_id from t_gestures";
	int id;

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	std::unordered_map<string, std::vector<string>>::iterator it;
	it = results.find("gesture_id");

	id = atoi(it->second[it->second.size() - 1].c_str());

	this->clearTable();

	return id;
}

/*
Function will edit the action of a gesture
*/
int InternalDB::editGestureAction(string fingers, string action)
{
	int rc;
	char *zErrMsg = 0;
	string message = "select gesture_id from t_gestures where";
	message = message + " fingers = ";
	message = message + "\'" + fingers + "\'";

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //No gesture was found
	{
		return 0;
	}

	int id = atoi(results.begin()->second[0].c_str());

	this->clearTable();

	message = "update t_gestures set action = ";
	message = message + "\'" + action + "\' where gesture_id = " + std::to_string(id);

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);
	this->clearTable();

	return id;
}