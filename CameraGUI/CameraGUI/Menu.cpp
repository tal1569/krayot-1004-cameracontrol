#include "Menu.h"

/*
Function is the c'tor of the class
Input: the parent, whether or not the user is connected to the server, the client and the guest
Output: none
*/
Menu::Menu(QWidget *parent, bool onlineMode, Client* client, Guest* guest) : QMainWindow()
{
	this->_ui.setupUi(this);
	this->_prevWindow = parent;
	this->_client = client;
	this->_guest = guest;
	if (onlineMode == true)
		this->_onlineMode = true;
	else
		this->_onlineMode = false;
}

/*
Function will handle the click on the "sign out" button
Input: none
Output: none
*/
void Menu::signOutHandle()
{
	if (this->_onlineMode == true)
	{
		this->_client->signOut();
	}
	MainWindow* main = new MainWindow();
	main->show();
	this->hide();
	delete this;
}

/*
Function will handle the click on the "add action" button
Input: none
Output: none
*/
void Menu::addActionHandle()
{
	if (this->_onlineMode == false) //Checking if the user is a client or guest
	{
		if (this->_guest->getAllLocalGesture().size() >= 10) //If it is a guest, he is only alowed up to 10 gestures
		{

			QMessageBox* messageBox = new QMessageBox(this);
			messageBox->setText("You can only have up to 10 gestures in this mode");
			messageBox->setWindowTitle("You reached the max number of gestures");
			messageBox->addButton(QMessageBox::Ok);

			messageBox->setStyleSheet("image:none");
			messageBox->show();
			messageBox->exec();
		}
		else
		{
			AddAction* form = new AddAction(this, this->_onlineMode, this->_client, this->_guest);
			form->show();
			this->hide();
		}
	}
	else
	{
		AddAction* form = new AddAction(this, this->_onlineMode, this->_client, this->_guest);
		form->show();
		this->hide();
	}
}

/*
Function will handle the click on the "edit" button
Input: none
Output: none
*/
void Menu::editActionHandle()
{
	EditAction* form = new EditAction(this, this->_onlineMode, this->_client, this->_guest);
	form->show();
	this->hide();
}

/*
Function will handle the click on the "run camera" button
Input: none
Output: none
*/
void Menu::runCameraHandle()
{
	CameraWindow* main = new CameraWindow(this,this->_onlineMode,this->_client,this->_guest);
	main->show();
	this->hide();
}

/*
Function will handle the push of the "x" button
Input: none
Output: none
*/
void Menu::closeEvent(QCloseEvent *event)
{
	QMessageBox* messageBox = new QMessageBox(this);
	messageBox->setText("Are you sure?");
	messageBox->setWindowTitle("Exit");
	messageBox->addButton(QMessageBox::No);
	messageBox->addButton(QMessageBox::Yes);
	
	messageBox->setStyleSheet("image:none");
	messageBox->show();

	int resBtn = messageBox->exec();

	if (resBtn != (int)(QMessageBox::Yes))
	{
		event->ignore();
	}
	else 
	{
		if (this->_onlineMode == true)
			this->_client->signOut();

		event->accept();
	}
}