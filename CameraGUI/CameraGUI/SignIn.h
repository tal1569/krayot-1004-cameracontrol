#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_SignIn.h"
#include "Client.h"
#include "MainWindow.h"
#include "Header.h"
#include "Menu.h"
#include "ForgotPassword.h"

class Client;
class MainWindow;
class Menu;

class SignIn : public QMainWindow
{
	Q_OBJECT

public:
	SignIn(QWidget *parent = Q_NULLPTR);
	void signinStatus(std::string msgId);

private:
	Ui::SignInClass _ui;
	QWidget* _prevWindow;
	Client* _client;
	Menu* _menu;
	std::string _status;

private slots:
	void backHandle();
	void signInHandle();
	void openMenuHandle();
	void forgotPasswordHandle();

};

