#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_MainWindow.h"
#include "SignIn.h"
#include "SignUp.h"
#include "Menu.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = Q_NULLPTR);

private:
	Ui::MainWindowClass _ui;

private slots:
	void signInHandle();
	void signUpHandle();
	void guestModeHandle();
};
