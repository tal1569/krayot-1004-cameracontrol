/********************************************************************************
** Form generated from reading UI file 'Menu.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MENU_H
#define UI_MENU_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MenuClass
{
public:
    QWidget *centralwidget;
    QPushButton *AddAction;
    QPushButton *EditAction;
    QPushButton *RunCamera;
    QPushButton *Back;

    void setupUi(QMainWindow *MenuClass)
    {
        if (MenuClass->objectName().isEmpty())
            MenuClass->setObjectName(QStringLiteral("MenuClass"));
        MenuClass->resize(900, 600);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Resources/Resource Files/The_Eye.png"), QSize(), QIcon::Normal, QIcon::Off);
        MenuClass->setWindowIcon(icon);
        MenuClass->setStyleSheet(QStringLiteral("image: url(:/Resources/Resource Files/Menu.png);"));
        centralwidget = new QWidget(MenuClass);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        AddAction = new QPushButton(centralwidget);
        AddAction->setObjectName(QStringLiteral("AddAction"));
        AddAction->setGeometry(QRect(410, 400, 101, 51));
        AddAction->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        EditAction = new QPushButton(centralwidget);
        EditAction->setObjectName(QStringLiteral("EditAction"));
        EditAction->setGeometry(QRect(410, 340, 101, 51));
        EditAction->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        RunCamera = new QPushButton(centralwidget);
        RunCamera->setObjectName(QStringLiteral("RunCamera"));
        RunCamera->setGeometry(QRect(410, 280, 101, 51));
        RunCamera->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        Back = new QPushButton(centralwidget);
        Back->setObjectName(QStringLiteral("Back"));
        Back->setGeometry(QRect(410, 510, 101, 51));
        Back->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        MenuClass->setCentralWidget(centralwidget);

        retranslateUi(MenuClass);
        QObject::connect(Back, SIGNAL(clicked()), MenuClass, SLOT(signOutHandle()));
        QObject::connect(RunCamera, SIGNAL(clicked()), MenuClass, SLOT(runCameraHandle()));
        QObject::connect(EditAction, SIGNAL(clicked()), MenuClass, SLOT(editActionHandle()));
        QObject::connect(AddAction, SIGNAL(clicked()), MenuClass, SLOT(addActionHandle()));

        QMetaObject::connectSlotsByName(MenuClass);
    } // setupUi

    void retranslateUi(QMainWindow *MenuClass)
    {
        MenuClass->setWindowTitle(QApplication::translate("MenuClass", "Menu", nullptr));
        AddAction->setText(QString());
        EditAction->setText(QString());
        RunCamera->setText(QString());
        Back->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MenuClass: public Ui_MenuClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MENU_H
