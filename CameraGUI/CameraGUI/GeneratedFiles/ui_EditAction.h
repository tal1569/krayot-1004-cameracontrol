/********************************************************************************
** Form generated from reading UI file 'EditAction.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITACTION_H
#define UI_EDITACTION_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EditActionClass
{
public:
    QWidget *centralwidget;
    QPushButton *Back;
    QPushButton *Refresh;
    QPushButton *EditButton;
    QPushButton *DeleteButton;
    QListWidget *Gestures;

    void setupUi(QMainWindow *EditActionClass)
    {
        if (EditActionClass->objectName().isEmpty())
            EditActionClass->setObjectName(QStringLiteral("EditActionClass"));
        EditActionClass->resize(900, 600);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Resource/Resource Files/The_Eye.png"), QSize(), QIcon::Normal, QIcon::Off);
        EditActionClass->setWindowIcon(icon);
        EditActionClass->setStyleSheet(QStringLiteral("image: url(:/Resource/Resource Files/EditAction.png);"));
        centralwidget = new QWidget(EditActionClass);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        Back = new QPushButton(centralwidget);
        Back->setObjectName(QStringLiteral("Back"));
        Back->setGeometry(QRect(670, 420, 75, 51));
        Back->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        Refresh = new QPushButton(centralwidget);
        Refresh->setObjectName(QStringLiteral("Refresh"));
        Refresh->setGeometry(QRect(660, 330, 91, 61));
        Refresh->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        EditButton = new QPushButton(centralwidget);
        EditButton->setObjectName(QStringLiteral("EditButton"));
        EditButton->setGeometry(QRect(660, 180, 91, 51));
        EditButton->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        DeleteButton = new QPushButton(centralwidget);
        DeleteButton->setObjectName(QStringLiteral("DeleteButton"));
        DeleteButton->setGeometry(QRect(660, 250, 91, 61));
        DeleteButton->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        Gestures = new QListWidget(centralwidget);
        Gestures->setObjectName(QStringLiteral("Gestures"));
        Gestures->setGeometry(QRect(10, 60, 581, 451));
        Gestures->setStyleSheet(QStringLiteral("image:none;"));
        EditActionClass->setCentralWidget(centralwidget);

        retranslateUi(EditActionClass);
        QObject::connect(Refresh, SIGNAL(clicked()), EditActionClass, SLOT(refreshHandle()));
        QObject::connect(Back, SIGNAL(clicked()), EditActionClass, SLOT(backHandle()));
        QObject::connect(EditButton, SIGNAL(clicked()), EditActionClass, SLOT(editHandle()));
        QObject::connect(DeleteButton, SIGNAL(clicked()), EditActionClass, SLOT(deleteHandle()));

        QMetaObject::connectSlotsByName(EditActionClass);
    } // setupUi

    void retranslateUi(QMainWindow *EditActionClass)
    {
        EditActionClass->setWindowTitle(QApplication::translate("EditActionClass", "Edit Gesture", nullptr));
        Back->setText(QString());
        Refresh->setText(QString());
        EditButton->setText(QString());
        DeleteButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class EditActionClass: public Ui_EditActionClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITACTION_H
