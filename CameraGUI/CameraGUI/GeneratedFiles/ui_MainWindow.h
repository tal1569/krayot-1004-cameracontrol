/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QWidget *centralWidget;
    QPushButton *signInButton;
    QPushButton *signUpButton;
    QPushButton *GuestMode;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QStringLiteral("MainWindowClass"));
        MainWindowClass->resize(900, 600);
        QIcon icon;
        icon.addFile(QStringLiteral(":/MainWindow/Resource Files/The_Eye.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindowClass->setWindowIcon(icon);
        MainWindowClass->setAutoFillBackground(false);
        MainWindowClass->setStyleSheet(QStringLiteral("image: url(:/MainWindow/Resource Files/MainWindow.png);"));
        MainWindowClass->setAnimated(true);
        MainWindowClass->setDockOptions(QMainWindow::AllowTabbedDocks|QMainWindow::AnimatedDocks);
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        signInButton = new QPushButton(centralWidget);
        signInButton->setObjectName(QStringLiteral("signInButton"));
        signInButton->setGeometry(QRect(370, 310, 141, 61));
        signInButton->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none"));
        signUpButton = new QPushButton(centralWidget);
        signUpButton->setObjectName(QStringLiteral("signUpButton"));
        signUpButton->setGeometry(QRect(370, 400, 141, 51));
        signUpButton->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none"));
        GuestMode = new QPushButton(centralWidget);
        GuestMode->setObjectName(QStringLiteral("GuestMode"));
        GuestMode->setGeometry(QRect(370, 480, 141, 51));
        GuestMode->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none"));
        MainWindowClass->setCentralWidget(centralWidget);

        retranslateUi(MainWindowClass);
        QObject::connect(signInButton, SIGNAL(clicked()), MainWindowClass, SLOT(signInHandle()));
        QObject::connect(signUpButton, SIGNAL(clicked()), MainWindowClass, SLOT(signUpHandle()));
        QObject::connect(GuestMode, SIGNAL(clicked()), MainWindowClass, SLOT(guestModeHandle()));

        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "Initial Window", nullptr));
        signInButton->setText(QString());
        signUpButton->setText(QString());
        GuestMode->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
