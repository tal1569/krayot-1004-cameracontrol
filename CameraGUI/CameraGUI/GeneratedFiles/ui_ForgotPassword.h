/********************************************************************************
** Form generated from reading UI file 'ForgotPassword.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORGOTPASSWORD_H
#define UI_FORGOTPASSWORD_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ForgotPasswordClass
{
public:
    QWidget *centralwidget;
    QPushButton *sendForgotPassword;
    QPushButton *Back;
    QLineEdit *username;

    void setupUi(QMainWindow *ForgotPasswordClass)
    {
        if (ForgotPasswordClass->objectName().isEmpty())
            ForgotPasswordClass->setObjectName(QStringLiteral("ForgotPasswordClass"));
        ForgotPasswordClass->resize(600, 300);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Resources/Resource Files/The_Eye.png"), QSize(), QIcon::Normal, QIcon::Off);
        ForgotPasswordClass->setWindowIcon(icon);
        ForgotPasswordClass->setStyleSheet(QStringLiteral("image: url(:/Resources/Resource Files/ForgotPassword.png);"));
        centralwidget = new QWidget(ForgotPasswordClass);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        sendForgotPassword = new QPushButton(centralwidget);
        sendForgotPassword->setObjectName(QStringLiteral("sendForgotPassword"));
        sendForgotPassword->setGeometry(QRect(300, 152, 41, 31));
        sendForgotPassword->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        Back = new QPushButton(centralwidget);
        Back->setObjectName(QStringLiteral("Back"));
        Back->setGeometry(QRect(540, 0, 61, 41));
        Back->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        username = new QLineEdit(centralwidget);
        username->setObjectName(QStringLiteral("username"));
        username->setGeometry(QRect(30, 150, 231, 41));
        QFont font;
        font.setPointSize(14);
        username->setFont(font);
        username->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;\n"
"border:none;"));
        username->setMaxLength(128);
        ForgotPasswordClass->setCentralWidget(centralwidget);

        retranslateUi(ForgotPasswordClass);
        QObject::connect(sendForgotPassword, SIGNAL(clicked()), ForgotPasswordClass, SLOT(sendForgotPassword()));
        QObject::connect(Back, SIGNAL(clicked()), ForgotPasswordClass, SLOT(backHandle()));

        QMetaObject::connectSlotsByName(ForgotPasswordClass);
    } // setupUi

    void retranslateUi(QMainWindow *ForgotPasswordClass)
    {
        ForgotPasswordClass->setWindowTitle(QApplication::translate("ForgotPasswordClass", "Forgot Password", nullptr));
        sendForgotPassword->setText(QString());
        Back->setText(QString());
        username->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ForgotPasswordClass: public Ui_ForgotPasswordClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORGOTPASSWORD_H
