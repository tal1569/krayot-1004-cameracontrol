/********************************************************************************
** Form generated from reading UI file 'AddAction.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDACTION_H
#define UI_ADDACTION_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddActionClass
{
public:
    QWidget *centralWidget;
    QLabel *image;
    QPushButton *AddButton;
    QPushButton *Back;
    QPushButton *StartCameraButton;
    QLabel *label_2;
    QLabel *label;

    void setupUi(QMainWindow *AddActionClass)
    {
        if (AddActionClass->objectName().isEmpty())
            AddActionClass->setObjectName(QStringLiteral("AddActionClass"));
        AddActionClass->resize(900, 600);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Resources/Resource Files/The_Eye.png"), QSize(), QIcon::Normal, QIcon::Off);
        AddActionClass->setWindowIcon(icon);
        AddActionClass->setStyleSheet(QStringLiteral("image: url(:/Resources/Resource Files/AddAction.png);"));
        centralWidget = new QWidget(AddActionClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        image = new QLabel(centralWidget);
        image->setObjectName(QStringLiteral("image"));
        image->setGeometry(QRect(60, 40, 661, 501));
        image->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        AddButton = new QPushButton(centralWidget);
        AddButton->setObjectName(QStringLiteral("AddButton"));
        AddButton->setGeometry(QRect(770, 190, 91, 61));
        AddButton->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        Back = new QPushButton(centralWidget);
        Back->setObjectName(QStringLiteral("Back"));
        Back->setGeometry(QRect(780, 380, 75, 51));
        Back->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        StartCameraButton = new QPushButton(centralWidget);
        StartCameraButton->setObjectName(QStringLiteral("StartCameraButton"));
        StartCameraButton->setGeometry(QRect(770, 290, 91, 61));
        StartCameraButton->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(270, 560, 251, 31));
        QFont font;
        font.setPointSize(16);
        label_2->setFont(font);
        label_2->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 560, 371, 31));
        label->setFont(font);
        label->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        AddActionClass->setCentralWidget(centralWidget);

        retranslateUi(AddActionClass);
        QObject::connect(AddButton, SIGNAL(clicked()), AddActionClass, SLOT(addHandle()));
        QObject::connect(Back, SIGNAL(clicked()), AddActionClass, SLOT(backHandle()));
        QObject::connect(StartCameraButton, SIGNAL(clicked()), AddActionClass, SLOT(startCamera()));

        QMetaObject::connectSlotsByName(AddActionClass);
    } // setupUi

    void retranslateUi(QMainWindow *AddActionClass)
    {
        AddActionClass->setWindowTitle(QApplication::translate("AddActionClass", "Add Action", nullptr));
        image->setText(QString());
        AddButton->setText(QString());
        Back->setText(QString());
        StartCameraButton->setText(QString());
        label_2->setText(QApplication::translate("AddActionClass", "Q - Quit", nullptr));
        label->setText(QApplication::translate("AddActionClass", "B- Refresh Background", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AddActionClass: public Ui_AddActionClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDACTION_H
