/********************************************************************************
** Form generated from reading UI file 'SignIn.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIGNIN_H
#define UI_SIGNIN_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SignInClass
{
public:
    QAction *actiondfs;
    QWidget *centralwidget;
    QPushButton *Back;
    QPushButton *SignIn;
    QLabel *Test;
    QPushButton *MenuButton;
    QPushButton *ForgotPassword;
    QLineEdit *Username;
    QLineEdit *Password;

    void setupUi(QMainWindow *SignInClass)
    {
        if (SignInClass->objectName().isEmpty())
            SignInClass->setObjectName(QStringLiteral("SignInClass"));
        SignInClass->setWindowModality(Qt::NonModal);
        SignInClass->resize(600, 299);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Resources/Resource Files/The_Eye.png"), QSize(), QIcon::Normal, QIcon::Off);
        SignInClass->setWindowIcon(icon);
        SignInClass->setStyleSheet(QStringLiteral("image: url(:/Resources/Resource Files/Login_without_Next.png);"));
        SignInClass->setInputMethodHints(Qt::ImhNone);
        actiondfs = new QAction(SignInClass);
        actiondfs->setObjectName(QStringLiteral("actiondfs"));
        centralwidget = new QWidget(SignInClass);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        Back = new QPushButton(centralwidget);
        Back->setObjectName(QStringLiteral("Back"));
        Back->setGeometry(QRect(544, 2, 51, 31));
        Back->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        SignIn = new QPushButton(centralwidget);
        SignIn->setObjectName(QStringLiteral("SignIn"));
        SignIn->setGeometry(QRect(410, 140, 121, 41));
        SignIn->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        Test = new QLabel(centralwidget);
        Test->setObjectName(QStringLiteral("Test"));
        Test->setGeometry(QRect(40, 260, 81, 20));
        Test->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        MenuButton = new QPushButton(centralwidget);
        MenuButton->setObjectName(QStringLiteral("MenuButton"));
        MenuButton->setGeometry(QRect(160, 230, 91, 71));
        MenuButton->setStyleSheet(QLatin1String("background-color: transparent;\n"
"border:none;\n"
"image: url(:/Resources/Resource Files/Next_Button.png);"));
        ForgotPassword = new QPushButton(centralwidget);
        ForgotPassword->setObjectName(QStringLiteral("ForgotPassword"));
        ForgotPassword->setGeometry(QRect(410, 190, 121, 41));
        ForgotPassword->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        Username = new QLineEdit(centralwidget);
        Username->setObjectName(QStringLiteral("Username"));
        Username->setGeometry(QRect(90, 130, 261, 31));
        Username->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;\n"
"border:none;"));
        Username->setMaxLength(128);
        Password = new QLineEdit(centralwidget);
        Password->setObjectName(QStringLiteral("Password"));
        Password->setGeometry(QRect(90, 200, 251, 31));
        Password->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;\n"
"border:none;"));
        Password->setMaxLength(128);
        Password->setEchoMode(QLineEdit::Password);
        SignInClass->setCentralWidget(centralwidget);

        retranslateUi(SignInClass);
        QObject::connect(SignIn, SIGNAL(clicked()), SignInClass, SLOT(signInHandle()));
        QObject::connect(Back, SIGNAL(clicked()), SignInClass, SLOT(backHandle()));
        QObject::connect(MenuButton, SIGNAL(clicked()), SignInClass, SLOT(openMenuHandle()));
        QObject::connect(ForgotPassword, SIGNAL(clicked()), SignInClass, SLOT(forgotPasswordHandle()));

        QMetaObject::connectSlotsByName(SignInClass);
    } // setupUi

    void retranslateUi(QMainWindow *SignInClass)
    {
        SignInClass->setWindowTitle(QApplication::translate("SignInClass", "Sign In", nullptr));
        actiondfs->setText(QApplication::translate("SignInClass", "dfs", nullptr));
        Back->setText(QString());
        SignIn->setText(QString());
        Test->setText(QString());
        MenuButton->setText(QString());
        ForgotPassword->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class SignInClass: public Ui_SignInClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIGNIN_H
