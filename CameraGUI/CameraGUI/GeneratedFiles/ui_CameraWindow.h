/********************************************************************************
** Form generated from reading UI file 'CameraWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERAWINDOW_H
#define UI_CAMERAWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CameraWindowClass
{
public:
    QWidget *centralwidget;
    QLabel *image;
    QPushButton *start;
    QPushButton *BackButton;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QMainWindow *CameraWindowClass)
    {
        if (CameraWindowClass->objectName().isEmpty())
            CameraWindowClass->setObjectName(QStringLiteral("CameraWindowClass"));
        CameraWindowClass->resize(903, 642);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Resource/Resource Files/The_Eye.png"), QSize(), QIcon::Normal, QIcon::Off);
        CameraWindowClass->setWindowIcon(icon);
        CameraWindowClass->setStyleSheet(QStringLiteral("image: url(:/Resource/Resource Files/CameraWindow.png);"));
        centralwidget = new QWidget(CameraWindowClass);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        image = new QLabel(centralwidget);
        image->setObjectName(QStringLiteral("image"));
        image->setGeometry(QRect(60, 60, 661, 481));
        image->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        start = new QPushButton(centralwidget);
        start->setObjectName(QStringLiteral("start"));
        start->setGeometry(QRect(770, 210, 91, 61));
        start->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        BackButton = new QPushButton(centralwidget);
        BackButton->setObjectName(QStringLiteral("BackButton"));
        BackButton->setGeometry(QRect(780, 320, 81, 61));
        BackButton->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(80, 570, 371, 31));
        QFont font;
        font.setPointSize(16);
        label->setFont(font);
        label->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(320, 570, 251, 31));
        label_2->setFont(font);
        label_2->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        CameraWindowClass->setCentralWidget(centralwidget);

        retranslateUi(CameraWindowClass);
        QObject::connect(start, SIGNAL(clicked()), CameraWindowClass, SLOT(startHandle()));
        QObject::connect(BackButton, SIGNAL(clicked()), CameraWindowClass, SLOT(backHandle()));

        QMetaObject::connectSlotsByName(CameraWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *CameraWindowClass)
    {
        CameraWindowClass->setWindowTitle(QApplication::translate("CameraWindowClass", "Camera Window", nullptr));
        image->setText(QString());
        start->setText(QString());
        BackButton->setText(QString());
        label->setText(QApplication::translate("CameraWindowClass", "B- Refresh Background", nullptr));
        label_2->setText(QApplication::translate("CameraWindowClass", "Q - Quit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CameraWindowClass: public Ui_CameraWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERAWINDOW_H
