/********************************************************************************
** Form generated from reading UI file 'SignUp.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIGNUP_H
#define UI_SIGNUP_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SignUpClass
{
public:
    QLabel *Test;
    QPushButton *Back;
    QPushButton *SignIn;
    QLineEdit *Username;
    QLineEdit *Password;
    QLineEdit *Email;

    void setupUi(QWidget *SignUpClass)
    {
        if (SignUpClass->objectName().isEmpty())
            SignUpClass->setObjectName(QStringLiteral("SignUpClass"));
        SignUpClass->resize(600, 300);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Resources/Resource Files/The_Eye.png"), QSize(), QIcon::Normal, QIcon::Off);
        SignUpClass->setWindowIcon(icon);
        SignUpClass->setStyleSheet(QStringLiteral("image: url(:/Resources/Resource Files/SignUp.png);"));
        Test = new QLabel(SignUpClass);
        Test->setObjectName(QStringLiteral("Test"));
        Test->setGeometry(QRect(30, 270, 331, 16));
        Test->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;\n"
"border:none;"));
        Back = new QPushButton(SignUpClass);
        Back->setObjectName(QStringLiteral("Back"));
        Back->setGeometry(QRect(490, 160, 101, 41));
        Back->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;\n"
"border:none;"));
        SignIn = new QPushButton(SignUpClass);
        SignIn->setObjectName(QStringLiteral("SignIn"));
        SignIn->setGeometry(QRect(480, 230, 111, 51));
        SignIn->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;\n"
"border:none;"));
        Username = new QLineEdit(SignUpClass);
        Username->setObjectName(QStringLiteral("Username"));
        Username->setGeometry(QRect(30, 109, 211, 21));
        Username->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;\n"
"border:none;"));
        Username->setMaxLength(128);
        Password = new QLineEdit(SignUpClass);
        Password->setObjectName(QStringLiteral("Password"));
        Password->setGeometry(QRect(30, 169, 211, 21));
        Password->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;\n"
"border:none;"));
        Password->setMaxLength(128);
        Email = new QLineEdit(SignUpClass);
        Email->setObjectName(QStringLiteral("Email"));
        Email->setGeometry(QRect(30, 230, 201, 31));
        Email->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;\n"
"border:none;"));
        Email->setMaxLength(128);

        retranslateUi(SignUpClass);
        QObject::connect(Back, SIGNAL(clicked()), SignUpClass, SLOT(backHandle()));
        QObject::connect(SignIn, SIGNAL(clicked()), SignUpClass, SLOT(signUpHandle()));

        QMetaObject::connectSlotsByName(SignUpClass);
    } // setupUi

    void retranslateUi(QWidget *SignUpClass)
    {
        SignUpClass->setWindowTitle(QApplication::translate("SignUpClass", "Sign Up", nullptr));
        Test->setText(QString());
        Back->setText(QString());
        SignIn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class SignUpClass: public Ui_SignUpClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIGNUP_H
