/********************************************************************************
** Form generated from reading UI file 'EditGesture.ui'
**
** Created by: Qt User Interface Compiler version 5.11.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITGESTURE_H
#define UI_EDITGESTURE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EditGestureClass
{
public:
    QWidget *centralwidget;
    QListWidget *listWidget;
    QPushButton *Browse;
    QPushButton *Back;
    QPushButton *OkButton;

    void setupUi(QMainWindow *EditGestureClass)
    {
        if (EditGestureClass->objectName().isEmpty())
            EditGestureClass->setObjectName(QStringLiteral("EditGestureClass"));
        EditGestureClass->resize(900, 600);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Resources/Resource Files/The_Eye.png"), QSize(), QIcon::Normal, QIcon::Off);
        EditGestureClass->setWindowIcon(icon);
        EditGestureClass->setStyleSheet(QStringLiteral("image: url(:/Resources/Resource Files/AddGestureWithoutBrowse.png);"));
        centralwidget = new QWidget(EditGestureClass);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        listWidget = new QListWidget(centralwidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(60, 80, 461, 461));
        listWidget->setStyleSheet(QStringLiteral("background-color: transparent;"));
        Browse = new QPushButton(centralwidget);
        Browse->setObjectName(QStringLiteral("Browse"));
        Browse->setGeometry(QRect(700, 450, 91, 61));
        Browse->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:url(:/Resources/Resource Files/Browse_Button.png)"));
        Back = new QPushButton(centralwidget);
        Back->setObjectName(QStringLiteral("Back"));
        Back->setGeometry(QRect(700, 370, 91, 51));
        Back->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        OkButton = new QPushButton(centralwidget);
        OkButton->setObjectName(QStringLiteral("OkButton"));
        OkButton->setGeometry(QRect(700, 280, 61, 61));
        OkButton->setStyleSheet(QLatin1String("background-color: transparent;\n"
"image:none;"));
        EditGestureClass->setCentralWidget(centralwidget);

        retranslateUi(EditGestureClass);
        QObject::connect(Back, SIGNAL(clicked()), EditGestureClass, SLOT(backHandle()));
        QObject::connect(listWidget, SIGNAL(itemPressed(QListWidgetItem*)), EditGestureClass, SLOT(listMarked()));
        QObject::connect(OkButton, SIGNAL(clicked()), EditGestureClass, SLOT(updateGestureAction()));
        QObject::connect(Browse, SIGNAL(clicked()), EditGestureClass, SLOT(browseHandle()));

        QMetaObject::connectSlotsByName(EditGestureClass);
    } // setupUi

    void retranslateUi(QMainWindow *EditGestureClass)
    {
        EditGestureClass->setWindowTitle(QApplication::translate("EditGestureClass", "Edit Action", nullptr));

        const bool __sortingEnabled = listWidget->isSortingEnabled();
        listWidget->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = listWidget->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("EditGestureClass", "Mouse right click", nullptr));
        QListWidgetItem *___qlistwidgetitem1 = listWidget->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("EditGestureClass", "Mouse left click", nullptr));
        QListWidgetItem *___qlistwidgetitem2 = listWidget->item(2);
        ___qlistwidgetitem2->setText(QApplication::translate("EditGestureClass", "Mouse double click", nullptr));
        QListWidgetItem *___qlistwidgetitem3 = listWidget->item(3);
        ___qlistwidgetitem3->setText(QApplication::translate("EditGestureClass", "Increase the volume of the computer", nullptr));
        QListWidgetItem *___qlistwidgetitem4 = listWidget->item(4);
        ___qlistwidgetitem4->setText(QApplication::translate("EditGestureClass", "Decrease the volume of the computer", nullptr));
        QListWidgetItem *___qlistwidgetitem5 = listWidget->item(5);
        ___qlistwidgetitem5->setText(QApplication::translate("EditGestureClass", "Mute the computer", nullptr));
        QListWidgetItem *___qlistwidgetitem6 = listWidget->item(6);
        ___qlistwidgetitem6->setText(QApplication::translate("EditGestureClass", "Unmute the computer", nullptr));
        QListWidgetItem *___qlistwidgetitem7 = listWidget->item(7);
        ___qlistwidgetitem7->setText(QApplication::translate("EditGestureClass", "Play mp3", nullptr));
        QListWidgetItem *___qlistwidgetitem8 = listWidget->item(8);
        ___qlistwidgetitem8->setText(QApplication::translate("EditGestureClass", "Play a playlist", nullptr));
        QListWidgetItem *___qlistwidgetitem9 = listWidget->item(9);
        ___qlistwidgetitem9->setText(QApplication::translate("EditGestureClass", "Open an app", nullptr));
        listWidget->setSortingEnabled(__sortingEnabled);

        Browse->setText(QString());
        Back->setText(QString());
        OkButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class EditGestureClass: public Ui_EditGestureClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITGESTURE_H
