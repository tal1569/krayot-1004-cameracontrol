#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <exception>
#include <thread>

#include "InternalDB.h"
#include "Camera.h"
#include "Gesture.h"
#include "WindowsControl.h"

#define CAMERA_INDEX 1

using std::cin;
using std::cout;
using std::endl;

class Guest
{
	private:
		int _cameraIndex;

		std::vector<Gesture*> _userGestures;
		InternalDB* _db;

	public:
		Guest(int cameraIndex);
		~Guest();

		void addGesture(Gesture* gesture);
		void deleteGesture(int gestureId);
		void editGesture(string action, string fingers);

		void updateCameraIndex(int cameraIndex);

		void compareGesture(Gesture* gesture);

		std::vector<Gesture*> getAllLocalGesture();
};

