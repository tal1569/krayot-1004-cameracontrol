#include "Header.h"


int main(int argc, char *argv[])
{
	WSAInitializer wsaInit;
	QApplication a(argc, argv);
	MainWindow w;


	w.show();
	return a.exec();
}

/*
	The function read data from config file
	Input: none.
	Output: none.
*/
vector<string> readConfigFile()
{
	std::map<std::string, std::string> store_line;
	std::fstream configFile;
	vector<string> values;
	configFile.open(CONFIG_FILE_NAME, std::fstream::in);
	if (configFile.is_open())
	{
		std::string line,lines = "";
		while (std::getline(configFile, line)) {
			lines = lines + line + '\n';
		}

		const char* config = lines.c_str();
		
		std::istringstream is_file(config);

		line = "";
		while (std::getline(is_file, line))
		{
			std::istringstream is_line(line);
			std::string key;
			if (std::getline(is_line, key, '='))
			{
				std::string value;
				if (std::getline(is_line, value))
				{
					store_line[key] = value;
					values.push_back(value);
				}
					
			}
		}
	}

	return values;
}