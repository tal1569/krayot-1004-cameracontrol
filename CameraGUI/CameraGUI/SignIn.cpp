#include "SignIn.h"

/*
Function is the c'tor of the class
Input: none
Output: none
*/
SignIn::SignIn(QWidget *parent) : QMainWindow()
{
	this->_ui.setupUi(this);
	this->_prevWindow = parent;
	this->_status = "";
	this->_menu = nullptr;

	this->_ui.MenuButton->hide();
}

/*
Function will handle the click on the "back" button
Input: none
Output: none
*/
void SignIn::backHandle()
{
	this->hide();
	this->_prevWindow->show();
	delete this;
}

/*
Function will handle the click on the "sign in" button
Input: none
Output: none
*/
void SignIn::signInHandle()
{
	QString username = this->_ui.Username->text();
	QString password = this->_ui.Password->text();


	try
	{
		this->_client = new Client(0, this);


		vector<string> ipAndPort = readConfigFile();
		this->_client->connect(ipAndPort[0], atoi(ipAndPort[1].c_str()));
		this->_client->startConversation();
		if (this->_client->signin(username.toStdString(), md5(password.toStdString())) == SOCKET_ERROR)
		{
			MainWindow* form = new MainWindow();
			QMessageBox* messageBox = new QMessageBox(form);
			messageBox->setText("Server crashed");
			messageBox->setWindowTitle("Can't connect to server");
			messageBox->addButton(QMessageBox::Ok);
			messageBox->setStyleSheet("image:none");
			messageBox->show();

			this->hide();
			form->show();
			delete this;
			return;
		}
	}
	catch (...)
	{
		///QMessageBox::StandardButton temp = QMessageBox::warning(this, "Can't connect to server", "Can't connect to server", QMessageBox::Ok);
		
		QMessageBox* messageBox = new QMessageBox(this);
		messageBox->setText("Can't connect to server");
		messageBox->setWindowTitle("Can't connect to server");
		messageBox->addButton(QMessageBox::Ok);
		messageBox->setStyleSheet("image:none");
		messageBox->show();
		messageBox->exec();
		///return;
	}

	
}

/*
Function will change the status of the 'sign in' attribute
Input: the message from the server
Output: none
*/
void SignIn::signinStatus(std::string msgId)
{
	///this->_ui.MenuButton->hide();
	//do msg box of status
	if (msgId[0] == '0')	//success
	{
		this->_status = "0";
		///this->_ui.Test->setText("Connected to Server");
		this->_ui.MenuButton->show();
		this->_ui.SignIn->setEnabled(false);
		this->_ui.Back->setEnabled(false);
	}
	else if (msgId[0] == '1')	//Wrong Details
	{
		this->_status = "1";
	}
	else if (msgId[0] == '2')	//User is already connected
	{
		this->_status = "2";
	}
	else //unknown error
	{

	}
}

/*
Function will handle the click on the "open menu" button
Input: none
Output: none
*/
void SignIn::openMenuHandle()
{
	if (this->_status.compare("0") == 0)
	{
		this->_menu = new Menu(this, true, this->_client, nullptr);
		this->_menu->show();
		this->hide();
		//delete this;
	}
	else if (this->_status.compare("1") == 0)	//Wrong Details
	{
		QMessageBox* messageBox = new QMessageBox(this);
		messageBox->setText("You have entered wrong details");
		messageBox->setWindowTitle("Error");
		messageBox->addButton(QMessageBox::Yes);
		QIcon icon("://Resources//Resource Files//Sad_Face.png");

		messageBox->setWindowIcon(icon);
		messageBox->setStyleSheet("image:none");
		messageBox->show();
		messageBox->exec();

	}
	else if(this->_status.compare("2") == 0) //User already connected to the server
	{
		QMessageBox* messageBox = new QMessageBox(this);
		messageBox->setText("User is already connected to the server");
		messageBox->setWindowTitle("Error");
		messageBox->addButton(QMessageBox::Yes);
		QIcon icon("://Resources//Resource Files//Sad_Face.png");

		messageBox->setWindowIcon(icon);
		messageBox->setStyleSheet("image:none");
		messageBox->show();
		messageBox->exec();
	}
	else //Unknown error
	{
		QMessageBox* messageBox = new QMessageBox(this);
		messageBox->setText("Unknown error has accured");
		messageBox->setWindowTitle("Error");
		messageBox->addButton(QMessageBox::Yes);
		QIcon icon("://Resources//Resource Files//Sad_Face.png");
		
		messageBox->setWindowIcon(icon);
		messageBox->setStyleSheet("image:none");
		messageBox->show();
		messageBox->exec();

	}
}

/*
Function will handle the click on the "forgot password" button
Input: none
Output: none
*/
void SignIn::forgotPasswordHandle()
{
	ForgotPassword* form = new ForgotPassword(this);
	form->show();
	this->hide();
}