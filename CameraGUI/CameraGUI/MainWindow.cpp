#include "MainWindow.h"

/*
Function is the c'tor of the class
Input: the parent
Output: none
*/
MainWindow::MainWindow(QWidget *parent)
	: QMainWindow()
{
	this->_ui.setupUi(this);
}

/*
Function will handle the click on the "sign in" button
Input: none
Output: none
*/
void MainWindow::signInHandle()
{
	SignIn* signin = new SignIn(this);
	signin->show();
	this->hide();
}

/*
Function will handle the click on the "sign up" button
Input: none
Output: none
*/
void MainWindow::signUpHandle()
{
	SignUp* signup = new SignUp(this);
	signup->show();
	this->hide();
}

/*
Function will handle the click on the "guest" button
Input: none
Output: none
*/
void MainWindow::guestModeHandle()
{
	Menu* menu = new Menu(this, false, nullptr, new Guest(0));	//need to cahnge the camera index to read from file elde put 0.
	menu->show();
	this->hide();
}