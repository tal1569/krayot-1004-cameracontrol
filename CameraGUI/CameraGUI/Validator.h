#pragma once

#include <string>

#define MIN_LENGTH 8
#define SPACE ' '
#define ZERO 0

using std::string;

bool isPasswordValid(string password);
bool isUsernameValid(string username);
bool isEmailValid(string email);