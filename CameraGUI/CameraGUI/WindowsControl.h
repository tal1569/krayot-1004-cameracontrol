#pragma once

#include <string>
#include <string.h>
#include <Windows.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <io.h>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <sys/stat.h>
#include <sys/types.h>
#include <direct.h>
#include <thread>


#define LEFT_CLICK "leftClick"
#define RIGHT_CLICK "rightClick"
#define DOUBLE_CLICK "doubleClick"
#define CREATE_FILE "createFile"
#define DELETE_FILE "deleteFile"
#define CREATE_DIRECTORY "createDirectory"
#define DELETE_DIRECTORY "deleteDirectory"
#define PLAYMP3 "playMP3"
#define PLAY_PLAYLIST "playPlaylist"
#define OPEN_APP "openApp"
#define INCREASE_VOLUNE "increaseVolume"
#define DECREASE_VOLUME "decreaseVolume"
#define MUTE "mute"
#define UNMUTE "unmute"
#define PAUSE "pause"


class WindowsControl
{
	public:

		void manageAction(std::string action);
		void moveMouse(int x, int y);

	private:
		bool leftClick();
		bool rightClick();
		bool doubleClick();

		bool playMP3(std::string path);
		bool playMP3Playlist(std::string folder);

		bool openApp(std::string path);

		void increaseVolume();
		void decreaseVolume();
		void mute();
		void unmute();
		
};