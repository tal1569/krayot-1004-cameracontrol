#include "EditGesture.h"

/*
Function is the c'tor of the class
Input: the parent, whether or not the user is connected to the server, the client and the guest
Output: none
*/
EditGesture::EditGesture(QWidget *parent, bool onlineMode, Client* client, Guest* guest,Gesture* gesture)
	: QMainWindow()
{
	this->_ui.setupUi(this);
	this->_ui.listWidget->setCurrentRow(0);

	this->_prevWindow = parent;
	this->_client = client;
	this->_guest = guest;
	this->_onlineMode = onlineMode;	//if true we need to use the client, else use the guest mode.
	this->_gesture = gesture;

	this->_ui.Browse->hide();

	this->_filename = "";

	this->_flag = true;
}

/*
Function is the d'tor of the class
*/
EditGesture::~EditGesture()
{
	//we do nothing here
}

/*
Function will handle the push on the "back" button
Input: none
Output: none
*/
void EditGesture::backHandle()
{
	this->hide();
	this->_prevWindow->show();
	delete this;
}

/*
Function will handle the push of the "x" button
Input: none
Output: none
*/
void EditGesture::closeEvent(QCloseEvent *event)
{
	QMessageBox* messageBox = new QMessageBox(this);
	messageBox->setText("Are you sure?");
	messageBox->setWindowTitle("Exit");
	messageBox->addButton(QMessageBox::No);
	messageBox->addButton(QMessageBox::Yes);

	messageBox->setStyleSheet("image:none");
	messageBox->show();

	int resBtn = messageBox->exec();

	if (resBtn != (int)(QMessageBox::Yes))
	{
		event->ignore();
	}
	else
	{
		if (this->_onlineMode == true)
			this->_client->signOut();

		event->accept();
	}
}

/*
Function will update the action of the gesture
Input: none
Output: none
*/
void EditGesture::updateGestureAction()
{
	if (this->_flag == true)
	{
		string action = "";
		string fingers = "";

		switch (this->_ui.listWidget->currentRow())
		{
		case 0:
			action = action + Helper::getPaddedNumber(10, 2) + RIGHT_CLICK;
			break;
		case 1:
			action = action + Helper::getPaddedNumber(9, 2) + LEFT_CLICK;
			break;
		case 2:
			action = action + Helper::getPaddedNumber(11, 2) + DOUBLE_CLICK;
			break;
		case 3:
			action = action + Helper::getPaddedNumber(14, 2) + INCREASE_VOLUNE;
			break;
		case 4:
			action = action + Helper::getPaddedNumber(14, 2) + DECREASE_VOLUME;
			break;
		case 5:
			action = action + Helper::getPaddedNumber(4, 2) + MUTE;
			break;
		case 6:
			action = action + Helper::getPaddedNumber(6, 2) + UNMUTE;
			break;
		case 7:
			action = action + Helper::getPaddedNumber(7, 2) + PLAYMP3 + Helper::getPaddedNumber(this->_filename.toStdString().size(), 3) + this->_filename.toStdString();
			break;
		case 8:
			action = action + Helper::getPaddedNumber(12, 2) + PLAY_PLAYLIST + Helper::getPaddedNumber(this->_filename.size(), 3) + this->_filename.toStdString();
			break;
		case 9:
			action = action + Helper::getPaddedNumber(7, 2) + OPEN_APP + Helper::getPaddedNumber(this->_filename.toStdString().size(), 3) + this->_filename.toStdString();
			break;
		default:
			action = "";
			break;
		}

		for (int i = 0; i < NUM_OF_FINGERS; i++)
		{
			if (this->_gesture->getFingers()[i] == true)
				fingers = fingers + std::to_string(i + 1);
			else
				fingers = fingers + "0";
		}

		if (this->_onlineMode == true)
		{
			if(this->_client->editAction(action, fingers) == SOCKET_ERROR)
			{
				MainWindow* form = new MainWindow();
				QMessageBox* messageBox = new QMessageBox(form);
				messageBox->setText("Server crashed");
				messageBox->setWindowTitle("Can't connect to server");
				messageBox->addButton(QMessageBox::Ok);
				messageBox->setStyleSheet("image:none");
				messageBox->show();

				
				this->hide();
				form->show();
				delete this;
				return;
			}
		}
		else
			this->_guest->editGesture(action, fingers);

		this->_gesture->setAction(action);

		///QMessageBox::information(this, "Action was changed", QString::fromStdString(action));

		QMessageBox* messageBox = new QMessageBox(this);
		messageBox->setText(QString::fromStdString(action));
		messageBox->setWindowTitle("Action was changed");
		messageBox->addButton(QMessageBox::Ok);

		messageBox->setStyleSheet("image:none");
		messageBox->show();

		messageBox->exec();
	}
	else
	{
		//message box that gesture didnt add.

		QMessageBox* messageBox = new QMessageBox(this);
		messageBox->setText(QString::fromStdString("Error while adding gesture"));
		messageBox->setWindowTitle("Can't add gesture");
		messageBox->addButton(QMessageBox::Ok);

		messageBox->setStyleSheet("image:none");
		messageBox->show();

		messageBox->exec();
	}

	//exit from this form
	this->hide();
	this->_prevWindow->show();
	delete this;
}

/*
Function will check which row was clicked and act according to that
Input: none
Output: none
*/
void EditGesture::listMarked()
{
	//Checking if we need to show the "browse" button or not

	if (this->_ui.listWidget->currentRow() < 10 && this->_ui.listWidget->currentRow() > 6)
	{
		//show the button
		this->_ui.Browse->show();
	}
	else
	{
		//hide
		this->_ui.Browse->hide();
	}

}

/*
Function will handle the click on the "browse" button
Input: none
Output: none
*/
void EditGesture::browseHandle()
{
	this->_filename = QFileDialog::getOpenFileName(this, "Open a file", "c://");
}

/*
Function will set the flag to false
Input: none
Output: none
*/
void EditGesture::setFlag()
{
	this->_flag = false;
}