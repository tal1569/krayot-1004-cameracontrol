#pragma once

#include <QtWidgets/qwidget.h>
#include <QCloseEvent>
#include <QMessageBox>

#include "ui_CameraWindow.h"
#include "Client.h"
#include "Guest.h"
#include "MainWindow.h"
#include "Header.h"
#include "Camera.h"

class Client;
class MainWindow;
class Guest;
class Camera;


class CameraWindow : public QMainWindow
{
	Q_OBJECT

public:
	CameraWindow(QWidget *parent, bool onlineMode, Client* client, Guest* guest);

	void setPicture(QImage* img);
	void setLabel(string msg);
private:
	Ui::CameraWindowClass _ui;
	QWidget* _prevWindow;
	Client* _client;
	Guest* _guest;
	bool _onlineMode;	//if true we need to use the client, else use the guest mode.
	Camera* _camera;
	bool _cameraFlag;

private slots:
	void signOutHandle();
	void closeEvent(QCloseEvent *event);
	void startHandle();
	void backHandle();

};

