#pragma once

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <exception>
#include <thread>
#include <vector>

#include "md5.h"
#include "Validator.h"
#include "InternalDB.h"
#include "Camera.h"
#include "Client.h"
#include "Guest.h"

#include "MainWindow.h"

#define HOST_IP "127.0.0.1"
#define HOST_PORT 8076

#define EXIT 0
#define ADD 1
#define DELETE 2
#define WATCH 3
#define LIVE 4
#define DEVELOPER 1273

#define CAMERA_INDEX 0

#define CONFIG_FILE_NAME "config.cfg"

using std::cin;
using std::cout;
using std::endl;
using std::vector;



vector<string> readConfigFile();