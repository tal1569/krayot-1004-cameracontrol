#pragma once

#include <QtWidgets/qwidget.h>
#include <QCloseEvent>
#include <QMessageBox>

#include "ui_ForgotPassword.h"
#include "Client.h"
#include "Guest.h"
#include "MainWindow.h"
#include "Header.h"
#include "CameraWindow.h"

class Client;
class MainWindow;

class ForgotPassword : public QMainWindow
{
	Q_OBJECT

public:
	ForgotPassword(QWidget *parent);
	~ForgotPassword();


private:
	Ui::ForgotPasswordClass _ui;
	Client* _client;
	QWidget* _prevWindow;


private slots:
	void sendForgotPassword();
	void closeEvent(QCloseEvent *event);
	void backHandle();
};

