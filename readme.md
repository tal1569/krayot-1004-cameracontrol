# Camera Control

Our project is a program which uses the computer's webcam in order to control different actions of the computer
with hand gestures only. A few defining features are:

* The program can identify different gestures that are being formed by a user's hand
* It can translate the gestures to commands for the computer
* It supports saving user-defined gestures and related commands on a remote server, but the program can also execute in guest mode

The program is written in c++ and was built using OpenCV and Qt.

## Getting Started

These instructions will get you a copy of the program up and running on your local machine.

### Prerequisites 

In order to run the program, you'll need to have:

* config.cfg
* nircmd.exe
* opencv_world342.dll
* Qt5Core.dll
* Qt5Gui.dll
* QtWidgets.dll
* haarcascade_frontalface_alt.xml

In addition, you can find in the same folder:

* CameraGUI.exe - the exe file that runs the program
* DataBase.db - The local database for guest mode
* playlist.m3u - A pre-populated playlist for you to try out

### Running the program locally

1. Run the CameraGui.exe
2. Choose whether you want to run as Guest or if you already have a user set up, choose "sign in".
3. Click "Run Camera"
4. You will see the webcam image
5. Show one hand to the camera
6. Perform a gesture - and watch the program execute a command!

For detailed instructions please see the flow charts

Our flow charts - We hope these will help you understand better, all written in the holy language :-)
* ![The first form flow](Photos/Annotation_2019-04-03_222352.png)
* ![The program flow chart](Photos/Menu.png)
* ![Hand detection](Photos/Annotation_2019-04-03_230602.png)

### Development set up

In order to set the development enviorment running you will need to follow this steps:
(All links will be at the end of the list)

* Firstly, you will need to have a place to run your codes. We suggest to use Visual Studio.
* After you installed VS, you'll have to add the opencv library to your project
* Lastly, you'll have to add the Qt addition in order to run the Gui

Links and stuff...

* [Visual Studio](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16) - The latest Visual Studio Community
* [Opencv](https://drive.google.com/file/d/12H3THSVTZDEn9OYMwALFCkpCq6PsO4_7/view) - A tutorial in the holy language :)
* [Qt](https://www.qt.io/download) - The official site for Qt
* [Qt tutorial](https://www.youtube.com/watch?v=I5jasWrsxT0) - a youtube tutorial for installing and setting up Qt

## Running the tests

For this example, we want to start a playlist which have already been linked to a gesture. The gesture is running a playlist
and in order to execute it, the user needs to open only his index and middle finger.
Let's see a a few screenshots which demonstrate the flow of events

* ![Running CameraGUI.exe](Photos/Annotation_2019-04-04_005454.png)
* ![Choosing the guest mode](Photos/Annotation_2019-04-04_005554.png)
* ![Running the camera](Photos/Annotation_2019-04-04_005656.png)
* ![Before showing the gesture](Photos/Annotation_2019-04-04_005825.png)
* ![After showing the gesture](Photos/Annotation_2019-04-04_010145.png)