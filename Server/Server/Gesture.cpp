#include "Gesture.h"

/*
Function is the c'tor of the class
Input: The number of fingers which are raised, which fingers are raised and the action of the gesture
Output: None
*/
Gesture::Gesture(int id, int numOfFingers, bool fingers[NUM_OF_FINGERS], string action)
{
	this->_id = id;
	this->_numOfFingers = numOfFingers;
	
	for (int i = 0; i < NUM_OF_FINGERS; i++)
	{
		this->_fingers[i] = fingers[i];
	}

	this->_action = action;
}

/*
Function is the d'tor of the class
Input: none
Output: none
*/
Gesture::~Gesture()
{
	///There is nothing to do here
}

/*
Function will compare the gesture to another one
Input: another gesture
Output: true or false
*/
bool Gesture::compare(Gesture other)
{
	if (this->_id != other.getId()) //Checking if the id is the same
	{
		return false;
	}

	if (this->_numOfFingers != other.getNumOfFingers()) //Checking if the same amount of fingers are risen
	{
		return false;
	}

	if (this->_action.compare(other.getAction()) != 0) //Checking if the 2 gestures do the same action
	{
		return false;
	}

	for (int i = 0; i < NUM_OF_FINGERS; i++) //Checking if the same fingers are risen
	{
		if (this->_fingers[i] != other.getFingers()[i])
		{
			return false;
		}
	}

	return true;
}

/*
Function will return the id of the gesture
Input: none
Output: the id of the gesture
*/
int Gesture::getId()
{
	return this->_id;
}

/*
Function will return the number of fingers risen
Input: none
Output: the amount of fingers risen
*/
int Gesture::getNumOfFingers()
{
	return this->_numOfFingers;
}

/*
Function will return the array which represents the fingers
Input: none
Output: the fingers
*/
bool* Gesture::getFingers()
{
	return this->_fingers;
}

/*
Function will return the action which the gesture needs to do
Input: none
Output: the action
*/
string Gesture::getAction()
{
	return this->_action;
}

/*
Function will return all the data of the gesture as a string
Input: none
Output: all the data of the gesture
*/
string Gesture::toString()
{
	string data = Helper::getPaddedNumber(this->_id, 4); //Adding the id as a 4 digit string to the data
	data = data + Helper::getPaddedNumber(this->_numOfFingers, 1); //Adding the number of fingers that are raised to the data

	for (int i = 0; i < NUM_OF_FINGERS; i++) //Adding the index of the fingers that are raised to the data
	{
		if (this->_fingers[i]) //If the finger is raised it will add the (index + 1) to the message
		{
			data = data + Helper::getPaddedNumber(i + 1, 1);
		}
		else //If not it will add 0 to the message
		{
			data = data + Helper::getPaddedNumber(0, 1);
		}
	}

	data = data + Helper::getPaddedNumber(this->_action.length(), 3); //Adding the length of the action to the data as a 3 digit string
	
	data = data + this->_action; //Adding the action to the data

	return data;
}