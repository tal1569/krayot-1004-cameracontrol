#include "DataBase.h"

unordered_map<string, vector<string>> results;

/*
Function is the c'tor of the DataBase
Input: none
Output: none
*/
DataBase::DataBase()
{
	char *zErrMsg = 0;
	int rc;
	rc = sqlite3_open(DATABASE_NAME, &this->_db);
	if (rc)
	{
		throw("Can't open the datebase");
	}

	//Creating the 2 tables
	rc = sqlite3_exec(this->_db, "CREATE TABLE t_users(username text primarykey not null, password text not null, email text not null)", callback, 0, &zErrMsg);
	rc = sqlite3_exec(this->_db, "CREATE TABLE t_gestures(gesture_id integer primary key autoincrement not null, user text not null, numOfFingers integer not null, fingers text not null, action text not null)", callback, 0, &zErrMsg);
}

/*
Function is the d'tor of the DataBase
Input: none
Output: none
*/
DataBase::~DataBase()
{
	sqlite3_close(this->_db);
}

/*
Function is a support function
*/
int DataBase::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

/*
Function is a support function
*/
void DataBase::clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

/*
Function will check if a user exists in the database
Input: the username
Output: true or false
*/
bool DataBase::isUserExists(string username)
{
	int rc;
	char *zErrMsg = 0;
	bool result;
	string message = "select * from t_users where username = ";
	message = message + "\'" + username + "\'";
	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //Checking if the user exists or not
	{
		result = false;
	}
	else
	{
		result = true;
	}

	this->clearTable();

	return result;
}

/*
Function will add a new user to the database
Input: the username, the password, the email
Output: true or false
*/
bool DataBase::addNewUser(string username, string password, string email)
{
	int rc;
	char *zErrMsg = 0;
	string message = "insert into t_users (username, password, email) values(";
	message = message + "\'" + username + "\'";
	message = message + ", " + "\'" + password + "\'";
	message = message + ", " + "\'" + email + "\'" + ")";


	if (this->isUserExists(username)) //Checking if a user with the username that was given to us already exists
	{
		return false;
	}

	rc = sqlite3_exec(this->_db, message.c_str(), callback, NULL, &zErrMsg);

	return true;
}

/*
Function will check if there is a someone with both the username and the password that was given to us
Input: the username and the password
Output: true or false
*/
bool DataBase::isUserAndPassMatch(string username, string password)
{
	int rc;
	char *zErrMsg = 0;
	string message = "select * from t_users where username = ";
	message = message + "\'" + username + "\'";
	message = message + " and password = ";
	message = message + "\'" + password + "\'";

	bool result;

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //Checking if there is no match
	{
		result = false;
	}
	else
	{
		result = true;
	}

	this->clearTable();

	return result;
}

/*
Function will return all the gestures that belong to a certain username
Input: the username who's gestures we want to return
Output: all the gestures of the user
*/
vector <Gesture*> DataBase::getAllGestures(string username)
{
	int rc;
	char *zErrMsg = 0;
	string message = "select * from t_gestures where user = ";
	message = message + "\'" + username + "\'";
	vector<Gesture*> gestures;
	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //We return an empty vector because there are no gestures for the user
	{
		this->clearTable();
		return gestures;
	}
	else
	{
		auto findId = results.find("gesture_id");
		auto findNumOfFingers = results.find("numOfFingers");
		auto findFingers = results.find("fingers");
		auto findAction = results.find("action");

		for (int i = 0; i < findId->second.size(); i++)
		{
			int id = atoi(findId->second[i].c_str());
			int numOfFingers = atoi(findNumOfFingers->second[i].c_str());
			string fingers = findFingers->second[i];
			bool raisedFingers[NUM_OF_FINGERS];

			for (int j = 0; j < NUM_OF_FINGERS; j++)
			{
				if (fingers[j] != '0')
				{
					raisedFingers[j] = true;
				}
				else
				{
					raisedFingers[j] = false;
				}
			}

			string action = findAction->second[i];

			Gesture* gesture = new Gesture(id, numOfFingers, raisedFingers, action);

			gestures.push_back(gesture);
		}

		this->clearTable();

		return gestures;
	}
}

/*
Function will check if the gesture already exists in the database
Input: the username, path, number of fingers raised, the fingers raised, the action
Output: true or false
*/
bool DataBase::doesGestureExists(string username, int numOfFingersRaised, string fingersRaised)
{
	int rc;
	char *zErrMsg = 0;

	string message = "select * from t_gestures where user = ";
	message = message + "\'" + username + "\' and ";
	message = message + "numOfFingers = " + to_string(numOfFingersRaised) + " and ";
	message = message + "fingers = \'" + fingersRaised + "\'";

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //The gesture does not exist in the database
	{
		return false;
	}

	this->clearTable();
	return true;
}

/*
Function will create a new gesture and add it to the database
Input: the username, the path, the number of fingers raised, the fingers raised, the action
Output: true or false
*/
bool DataBase::createGesture(string username, int numOfFingersRaised, string fingersRaised, string action)
{
	int rc;
	char *zErrMsg = 0;

	if (!this->doesGestureExists(username, numOfFingersRaised, fingersRaised))
	{
		string message = "insert into t_gestures(user, numOfFingers, fingers, action) values(";
		message = message + "\'" + username + "\', " + to_string(numOfFingersRaised) + ", \'" + fingersRaised + "\', " + "\'" + action + "\')";

		try
		{
			rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);
			this->clearTable();
			return true;
		}
		catch (...)
		{
			return false;
		}
	}

	return false;
}

/*
Function will check if the gesture belongs to the user
Input: the user and the path of the gesture
Output: true or false
*/
bool DataBase::doesGestureBelongToUser(string username, int id)
{
	int rc;
	char *zErrMsg = 0;

	string message = "select * from t_gestures where user = \'" + username + "\' and gesture_id = " + to_string(id);

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //The gesture does not belong to the user
	{
		return false;
	}

	this->clearTable();
	return true;
}

/*
Function will delete a gesture from the database
Input: the path of the gesture
Output: true or false
*/
int DataBase::deleteGesture(int gestureId)
{
	int rc;
	char *zErrMsg = 0;

	int id;
	string getId = "select gesture_id from t_gestures where gesture_id = " + to_string(gestureId);
	string message = "delete from t_gestures where gesture_id = " + to_string(gestureId);

	try
	{
		rc = sqlite3_exec(this->_db, getId.c_str(), callback, 0, &zErrMsg);
		if (results.begin() == results.end())
		{
			this->clearTable();
			return 0;
		}

		id = atoi(results.begin()->second[0].c_str());

		this->clearTable();

		rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

		this->clearTable();
		return id;
	}
	catch (...)
	{
		return 0;
	}
}

/*
Function will return the id of the last gesture in the database
Input: none
Output: the id
*/
int DataBase::getLastGestureId()
{
	int rc;
	char *zErrMsg = 0;

	string message = "select gesture_id from t_gestures";
	int id;

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	id = atoi(results.begin()->second[results.begin()->second.size() - 1].c_str());

	this->clearTable();

	return id;
}

/*
Function will return the password of the user
Input: the username
Output: the password
*/
string DataBase::getUserPassword(string username)
{
	int rc;
	char *zErrMsg = 0;

	string message = "select password from t_users where username = ";
	message = message + "\'" + username + "\'";

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() != results.end())
	{
		string password = results.begin()->second[0];
		this->clearTable();
		return password;
	}

	this->clearTable();
	return "";
}

/*
Function will return the email of the user
Input: the username
Output: the email
*/
string DataBase::getUserEmail(string username)
{
	int rc;
	char *zErrMsg = 0;

	string message = "select email from t_users where username = ";
	message = message + "\'" + username + "\'";

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() != results.end())
	{
		string email = results.begin()->second[0];
		this->clearTable();
		return email;
	}

	this->clearTable();
	return "";
}

/*
Function will change the password of the user
Input: the username and the password
Output: none
*/
void DataBase::changeUserPassword(string username, string password)
{
	int rc;
	char *zErrMsg = 0;

	string message = "update t_users set password = ";
	message = message + "\'" + password + "\' where username = ";
	message = message + "\'" + username + "\'";

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	this->clearTable();
}

/*
Function will edit an action of a gesture
Input: the username, the fingers, the action
Output: the id of the edited gesture. If the gesture was not edited for some reason - then 0 will be returned
*/
int DataBase::editGestureAction(string username, string fingers, string action)
{
	int rc;
	char *zErrMsg = 0;
	string message = "select gesture_id from t_gestures where user = ";
	message = message + "\'" + username + "\' and fingers = ";
	message = message + "\'" + fingers + "\'";

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);

	if (results.begin() == results.end()) //No gesture was found
	{
		return 0;
	}

	int id = atoi(results.begin()->second[0].c_str());

	this->clearTable();

	message = "update t_gestures set action = ";
	message = message + "\'" + action + "\' where gesture_id = " + to_string(id);

	rc = sqlite3_exec(this->_db, message.c_str(), callback, 0, &zErrMsg);
	this->clearTable();

	return id;
}