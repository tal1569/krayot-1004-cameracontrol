#pragma once
#include <vector>
#include <string>
#include <WinSock2.h>
#include <map>
#include <unordered_map>
#include <queue>
#include <exception>
#include <thread>
#include <iostream> 
#include <mutex>

#include "RecievedMessage.h"
#include "DataBase.h"
#include "Validator.h"
#include "Gesture.h"
#include "md5.h"
#include <time.h>
#pragma comment (lib, "Ws2_32.lib")

#define PORT 8076
#define IFACE 0

using std::string;
using std::vector;
using std::map;
using std::queue;
using std::thread;
using std::mutex;
using std::unique_lock;
using std::unordered_map;

class DataBase;
class Game;

class Listen
{
private:
	map<SOCKET, User*> _connectedUsers;
	queue<RecievedMessage*> _queRcvMessages;

	DataBase* _db;

	SOCKET _listeningSocket;

	std::mutex _mtxRecievedMessages;
	std::condition_variable _msgQueueCondition;


	void bindAndListen();
	void acceptClient();

	void clientHandler(SOCKET client_socket);

	User* getUserByName(string username);

	User* getUserBySocket(SOCKET client_socket);

	void safeDeleteUser(RecievedMessage* msg);

	User* handleSignIn(RecievedMessage* msg);
	bool handleSignUp(RecievedMessage* msg);
	void handleSignOut(RecievedMessage* msg);

	void handleGetGestures(RecievedMessage* msg);
	void handleEditGesture(RecievedMessage* msg);
	void handleForgotPassword(RecievedMessage* msg);
	void handleCreateGesture(RecievedMessage* msg);
	void handleDeleteGesture(RecievedMessage* msg);


	void handleRecievedMessages();
	void addRecieveMessage(RecievedMessage* msg);
	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);

public:
	Listen();
	~Listen();

	void serve();

};