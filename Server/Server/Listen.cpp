#include "Listen.h"


/*
	The c'tor of TrivaServer
*/
Listen::Listen()
{
	//cell DB c'tor
	this->_db = new DataBase();

	//open socket...
	// notice that we step out to the global namespace
	// for the resolution of the function socket
	

	this->_listeningSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_listeningSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


/*
	The d'tor of TrivaServer
*/
Listen::~Listen()
{
	TRACE(__FUNCTION__ " closing accepting socket");
	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(this->_listeningSocket);
	}
	catch (...) {}
}

/*
	The function make thread for listening to clients that want to join.
	Input: none.
	Output: none.
*/
void Listen::serve()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&Listen::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		acceptClient();
	}
}

/*
	The function listening to join request to the server.
	Input: none.
	Output: none.
*/
void Listen::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(this->_listeningSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(this->_listeningSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");

}

/*
	The function accept client that join and make thread for the client.
	Input: none.
	Output: none
*/
void Listen::acceptClient()
{
	SOCKET client_socket = accept(this->_listeningSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	thread tr(&Listen::clientHandler, this, client_socket);
	tr.detach();
}

/*
	The function get the message from the client and call function to add the message to queue.
	Input: client socket.
	Output: none.
*/
void Listen::clientHandler(SOCKET client_socket)
{
	RecievedMessage* currMsg = nullptr;
	try
	{
		int messageCode = Helper::getMessageTypeCode(client_socket);

		while (messageCode != 0 && messageCode != atoi(EXIT_APP))
		{
			

			RecievedMessage* currMsg = buildRecieveMessage(client_socket, messageCode);

			
			addRecieveMessage(currMsg);

			messageCode = Helper::getMessageTypeCode(client_socket);

			std::cout << std::to_string(messageCode) << std::endl;
		}

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		currMsg = buildRecieveMessage(client_socket, atoi(EXIT_APP));
		addRecieveMessage(currMsg);
	}
	closesocket(client_socket);
}
/*
	The function take message from message queue and handling message.
	Input: none.
	Output: none.
*/
void Listen::handleRecievedMessages()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	RecievedMessage* currMessage = nullptr;

	srand(time(NULL));

	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMessages);

			// Wait for clients to enter the queue.
			if (_queRcvMessages.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (_queRcvMessages.empty())
				continue;

			RecievedMessage* currMessage = _queRcvMessages.front();
			_queRcvMessages.pop();
			lck.unlock();

			
			// Extract the data from the tuple.
			clientSock = currMessage->getSocket();

			currMessage->setUser(getUserBySocket(clientSock));

			msgCode = currMessage->getMessageID();

			string msgType = std::to_string(msgCode);

			if (msgType.compare(SIGN_IN) == 0)			//Message 200
			{
				User* user = handleSignIn(currMessage);

				if(user != nullptr)
					this->_connectedUsers[user->getSocket()] = user;
			}
			else if (msgType.compare(SIGN_OUT) == 0)	//Message 201
			{
				handleSignOut(currMessage);
			}
			else if (msgType.compare(SIGN_UP) == 0)		//Message 203
			{
				handleSignUp(currMessage);
			}
			else if (msgType.compare(GET_GESTURES) == 0)	//Messaeg 205
			{
				handleGetGestures(currMessage);
			}
			else if (msgType.compare(EDIT_GESTURE_ACTION) == 0) //Message 207
			{
				handleEditGesture(currMessage);
			}
			else if (msgType.compare(FORGOT_PASSWORD) == 0)	//Message 209
			{
				handleForgotPassword(currMessage);
			}
			else if (msgType.compare(CREATE_GESTURE) == 0)	//Message 213
			{
				handleCreateGesture(currMessage);
			}
			else if (msgType.compare(DELETE_GESTURE) == 0)	//Message 215
			{
				handleDeleteGesture(currMessage);
			}
			else if (msgType.compare(EXIT_APP) == 0)		//Message 299
			{
				safeDeleteUser(currMessage);
			}
			else
			{
				safeDeleteUser(currMessage);
			}

			

			delete currMessage;
		}
		catch (...)
		{
			safeDeleteUser(currMessage);
		}
	}
}

/*
	The function add message to message queue.
	Input: message to add.
	Output: none.
*/
void Listen::addRecieveMessage(RecievedMessage* msg)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);

	_queRcvMessages.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();
}

/*
	The function find the user by his name.
	Input: username.
	Output: user.
*/
User* Listen::getUserByName(string username)
{
	for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if (it->second != nullptr)
		{
			if (it->second->getUsername().compare(username) == 0)
			{
				return it->second;
			}
		}
	}

	return nullptr;
}

/*
	The function get the user by the socket.
	Input: user socket.
	output: user.
*/
User* Listen::getUserBySocket(SOCKET client_socket)
{
	for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if (it->first == client_socket)
		{
			return it->second;
		}
	}
	return nullptr;
}

/*
	The function delete user from the server.
	Input: client message.
	Output: none.
*/
void Listen::safeDeleteUser(RecievedMessage* msg)
{
	try
	{
		SOCKET user_sock = msg->getSocket();

		handleSignOut(msg);

		this->_connectedUsers.erase(user_sock);
	}
	catch (...)
	{

	}
}

/*
	The funcion Sign in user and add him to the server, send to the user if success or fail
	Input: user message
	Output: pointer to user that added or nullptr.
*/
User* Listen::handleSignIn(RecievedMessage* msg)
{
	User* user;
	string username = msg->getMessage()[0];
	string password = msg->getMessage()[1];

	bool flag = _db->isUserAndPassMatch(username, password);

	if (flag)	//username and password right.
	{
		user = getUserByName(username);

		if (user == nullptr)	//the user doesn't connected to the server
		{
			user = new User(username,msg->getSocket());
			this->_connectedUsers[msg->getSocket()] = user;

			Helper::sendData(msg->getSocket(), SIGN_IN_SUCCESS);

			return user;

		}
		else	//user already CONNECTED
		{
			Helper::sendData(msg->getSocket(), SIGN_IN_USER_IS_ALREADY_CONNECTED);
			user = nullptr;
		}

	}
	else	//username or password incorrect
	{
		Helper::sendData(msg->getSocket(), SIGN_IN_WRONG_DETAILS);
		user = nullptr;
	}

	return user;
}

/*
	The function SignUp the user to the database of the server.
	Input: user message to signup with all details.
	Output: true or false if success or fail.
*/
bool Listen::handleSignUp(RecievedMessage* msg)
{
	string username = msg->getMessage()[0];
	string password = msg->getMessage()[1];
	string email = msg->getMessage()[2];
	

	if (!this->_db->isUserExists(username))			//check if the username not Catch by anyone else
	{
		bool check = this->_db->addNewUser(username, password, email);

		if (check)
		{
			Helper::sendData(msg->getSocket(), SIGN_UP_SUCCESS);
			return true;
		}
		else
		{
			Helper::sendData(msg->getSocket(), SIGN_UP_OTHER);
			return false;
		}
	}
	else
	{
		Helper::sendData(msg->getSocket(), SIGN_UP_USERNAME_ALREADY_EXISTS);
		return false;
	}

	return false;
}

/*
	The function SignOut user from the server.
	Input: user message to signout.
	Output: none.
*/
void Listen::handleSignOut(RecievedMessage* msg)
{
	User* user = msg->getUser();
	if(user != NULL)
		this->_connectedUsers.erase(user->getSocket());
}

/*
	The function gets all the gestures from DB
	Input: user message
	Output: none
*/
void Listen::handleGetGestures(RecievedMessage* msg)
{
	User* user = msg->getUser();
	string message = SEND_GESTURES;
	vector<Gesture*> gestures = this->_db->getAllGestures(user->getUsername());
	message = message + Helper::getPaddedNumber(gestures.size(), 4);

	for (int i = 0; i < gestures.size(); i++)
	{
		message = message + gestures[i]->toString();
	}

	user->send(message);
}

/*
Function will generate a new password for the user and send it to him by mail
Input: user message
Output: none
*/
void Listen::handleForgotPassword(RecievedMessage* msg)
{
	string message;
	string username = msg->getMessage()[0];

	if (!(this->_db->isUserExists(username)))
	{
		message = FORGOT_PASSWORD_FAILED_USER_DOES_NOT_EXIST_OR_OTHER_REASON;
	}
	else
	{
		try
		{
			string email = this->_db->getUserEmail(username);
			std::string newPass = "";

			std::string alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

			for (int i = 0; i < 10; ++i) {
				newPass = newPass + alphanum[rand() % ((alphanum).size() / sizeof(alphanum[0]) - 1)];
			}

			newPass[10] = 0;

			string command = "SendEmail.exe " + email + " " + (string)newPass;
			system(command.c_str());

			this->_db->changeUserPassword(username, md5((string)newPass));

			message = FORGOT_PASSWORD_SUCCESS;
		}
		catch (...)
		{
			message = FORGOT_PASSWORD_FAILED_COULD_NOT_SEND_EMAIL;
		}
	}
}

/*
Function will edit an action of a gesture that belongs to the user
Input: user message
Output: none
*/
void Listen::handleEditGesture(RecievedMessage* msg)
{
	User* user = msg->getUser();
	string username = msg->getMessage()[0];
	string fingers = msg->getMessage()[1];
	string action = msg->getMessage()[2];

	int id = this->_db->editGestureAction(username, fingers, action);

	string message = EDIT_GESTURE_ACTION_ANSWER + Helper::getPaddedNumber(to_string(id).size(), 2) + to_string(id) + Helper::getPaddedNumber(action.size(), 2) + action;

	user->send(message);
}

/*
	Function asks the DB to create a new gesture
	Input: user message
	Output: none
*/
void Listen::handleCreateGesture(RecievedMessage* msg)
{
	User* user = msg->getUser();
	string message;

	string username = msg->getMessage()[0];
	int numOfFingersRaised = atoi(msg->getMessage()[1].c_str());
	string fingersRaised = msg->getMessage()[2];
	string action = msg->getMessage()[3];

	if (this->_db->createGesture(username, numOfFingersRaised, fingersRaised, action))
	{
		message = CREATE_GESTURE_SUCCESS + Helper::getPaddedNumber(to_string(this->_db->getLastGestureId()).size(), 2) + to_string(this->_db->getLastGestureId());
	}
	else
	{
		message = CREATE_GESTURE_FAIL;
	}

	user->send(message);
}

/*
Function will try to delete the gesture the user wants to delete
Input: the user message
Output: none
*/
void Listen::handleDeleteGesture(RecievedMessage* msg)
{
	User* user = msg->getUser();
	string message;

	string username = msg->getMessage()[0];
	int gestureId = atoi(msg->getMessage()[1].c_str());

	try
	{
		if (this->_db->doesGestureBelongToUser(username, gestureId))
		{
			int id = this->_db->deleteGesture(gestureId);

			if (id != 0)
			{
				message = DELETE_GESUTRE_SUCCESS + Helper::getPaddedNumber(to_string(id).size(), 2) + to_string(id);
			}
			else
			{
				message = DELETE_GESUTRE_FAIL_OTHER_REASON;
			}
		}
		else
		{
			message = DELETE_GESUTRE_FAIL_GESTURE_DOES_NOT_BELONG_TO_USER;
		}
	}
	catch (...)
	{
		message = DELETE_GESUTRE_FAIL_OTHER_REASON;
	}

	user->send(message);
}

/*
	The function build the RecievedMessage from message that user send.
	Input: user message.
	Output: RecievedMessage
*/
RecievedMessage* Listen::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	RecievedMessage* msg = nullptr;
	vector<string> message;
	User* user = getUserBySocket(client_socket);

	if (std::to_string(msgCode).compare(SIGN_IN) == 0)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		int passwordSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passwordSize);
		message.push_back(userName);
		message.push_back(password);
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(SIGN_OUT) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(SIGN_UP) == 0)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string UserName = Helper::getStringPartFromSocket(client_socket, userSize);

		int passSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passSize);

		int emailSize = Helper::getIntPartFromSocket(client_socket, 2);
		string email = Helper::getStringPartFromSocket(client_socket, emailSize);

		message.push_back(UserName);
		message.push_back(password);
		message.push_back(email);

		user = new User(UserName, client_socket);

		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(GET_GESTURES) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(EDIT_GESTURE_ACTION) == 0)
	{
		int usernameSize = Helper::getIntPartFromSocket(client_socket, 2);
		string username = Helper::getStringPartFromSocket(client_socket, usernameSize);

		string fingers = Helper::getStringPartFromSocket(client_socket, 5);

		int actionSize = Helper::getIntPartFromSocket(client_socket, 3);
		string action = Helper::getStringPartFromSocket(client_socket, actionSize);

		message.push_back(username);
		message.push_back(fingers);
		message.push_back(action);

		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(FORGOT_PASSWORD) == 0)
	{
		int usernameSize = Helper::getIntPartFromSocket(client_socket, 2);
		string username = Helper::getStringPartFromSocket(client_socket, usernameSize);

		message.push_back(username);

		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(CREATE_GESTURE) == 0)
	{
		int usernameSize = Helper::getIntPartFromSocket(client_socket, 2);
		string username = Helper::getStringPartFromSocket(client_socket, usernameSize);

		string numOfFingersRaised = Helper::getStringPartFromSocket(client_socket, 1);
		string fingersRaised = Helper::getStringPartFromSocket(client_socket, 5);

		int commandNameSize = Helper::getIntPartFromSocket(client_socket, 3);
		string commandName = Helper::getStringPartFromSocket(client_socket, commandNameSize);

		message.push_back(username);
		message.push_back(numOfFingersRaised);
		message.push_back(fingersRaised);
		message.push_back(commandName);
			
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(DELETE_GESTURE) == 0)
	{
		int usernameSize = Helper::getIntPartFromSocket(client_socket, 2);
		string username = Helper::getStringPartFromSocket(client_socket, usernameSize);

		int idSize = Helper::getIntPartFromSocket(client_socket, 3);
		string id = Helper::getStringPartFromSocket(client_socket, idSize);

		message.push_back(username);
		message.push_back(id);

		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}
	else if (std::to_string(msgCode).compare(EXIT_APP) == 0)
	{
		msg = new RecievedMessage(user, client_socket, std::to_string(msgCode), message);
	}

	for (int i = 0; i < msg->getMessage().size(); i++)
	{
		cout << msg->getMessage()[i];
		cout << " ";
	}
	return msg;
}