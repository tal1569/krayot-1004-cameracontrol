#pragma once

#include "User.h"
#include <vector>

using std::vector;

class RecievedMessage
{
private:
	User* _user;
	SOCKET _userSocket;
	string _messageID;
	vector<string> _message;

public:

	RecievedMessage(User* user, SOCKET userSocket, string messageID, vector<string> message);
	~RecievedMessage();
	int getMessageID();
	vector<string> getMessage();
	SOCKET getSocket();
	User* getUser();

	void setUser(User* user);
};