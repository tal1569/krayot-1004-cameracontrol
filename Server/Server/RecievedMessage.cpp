#include "RecievedMessage.h"

/*
	The c'tor of RecievedMessage
*/
RecievedMessage::RecievedMessage(User* user, SOCKET userSocket, string messageID, vector<string> message)
{
	this->_message = message;
	this->_messageID = messageID;
	this->_user = user;
	this->_userSocket = userSocket;
}

/*
	The d'tor of RecievedMessage
*/
RecievedMessage::~RecievedMessage()
{

}

/*
	The function return the message id
	Input: none.
	Output: message id
*/
int RecievedMessage::getMessageID()
{
	return std::atoi(this->_messageID.c_str());
}

/*
The function return the message 
Input: none.
Output: message
*/
vector<string> RecievedMessage::getMessage()
{
	return this->_message;
}

/*
The function return the socket
Input: none.
Output: socket
*/
SOCKET RecievedMessage::getSocket()
{
	return this->_userSocket;
}

/*
The function return the user
Input: none.
Output: pointer to user
*/
User* RecievedMessage::getUser()
{
	return this->_user;
}

/*
	The function set the user of the message
	Input: user
	Output: none
*/
void RecievedMessage::setUser(User* user)
{
	this->_user = user;
}
