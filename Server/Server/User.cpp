#include "User.h"

/*
Function is the c'tor of the user
Input: the username and the socket
Output: none
*/
User::User(string username, SOCKET sock) 
{
	this->_username = username;
	this->_sock = sock;
}

/*
Function is the d'tor of the user
Input: none
Output: none
*/
User::~User()
{
	//There is nothing to do here
}

/*
Function will send a message to the user
Input: the message
Output: none
*/
void User::send(string message)
{
	Helper::sendData(this->_sock, message);
}

/*
Function will return the username
Input: none
Output: none
*/
string User::getUsername()
{
	return this->_username;
}

/*
Function will return the socket
Input: none
Output: the socket
*/
SOCKET User::getSocket()
{
	return this->_sock;
}